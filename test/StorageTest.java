import org.junit.*;

import static org.junit.Assert.*;
import java.io.Serializable;
import java.io.File;

import tp.impl.Storage;
import tp.Lockable;
import tp.RID;
import tp.Transaction;

public class StorageTest {
    private File dataDir = new File("./test_data");

    @Before public void setUp() {
        dataDir.mkdirs();
    }

    @Test public void simpleCase() throws Exception {
        Storage<RID,String> mStore = new Storage<RID,String>("test",dataDir);
        RID first = new RID(1);
        RID second = new RID(2);
        Transaction ctx = new Transaction();

        mStore.write(ctx, first, "Alpha");
        mStore.write(ctx, second, "Beta");
        mStore.prepare(ctx);
        mStore.commit(ctx);

        assertEquals( mStore.read(ctx, first), "Alpha");
        assertEquals( mStore.read(ctx, second), "Beta");
    }

    @Test public void simpleAbort() throws Exception {
        Storage<RID,String> mStore = new Storage<RID,String>("test",dataDir);
        RID first = new RID(1);
        RID second = new RID(2);
        Transaction ctx = new Transaction();

        mStore.write(ctx, first, "Alpha");
        mStore.write(ctx, second, "Beta");
        mStore.prepare(ctx);
        mStore.commit(ctx);

        mStore.write(ctx, first, "Gamma");
        mStore.abort(ctx);

        ctx = new Transaction();
        assertEquals( "Alpha", mStore.read(ctx, first));
        assertEquals( "Beta", mStore.read(ctx, second));
    }
    
    @Test public void simpleConcurrency() throws Exception {
        Storage<RID,String> mStore = new Storage<RID,String>("test",dataDir);
        RID first = new RID(1);
        RID second = new RID(2);
        Transaction ctx1 = new Transaction();
        Transaction ctx2 = new Transaction();

        mStore.write(ctx1, first, "Alpha");
        mStore.write(ctx1, second, "Beta");

        mStore.write(ctx2, first, "Gamma");
        assertEquals( "Alpha", mStore.read(ctx1, first));

        mStore.prepare(ctx1);
        mStore.commit(ctx1);
        mStore.prepare(ctx2);
        mStore.commit(ctx2);

        Transaction ctx = new Transaction();
        assertEquals( "Gamma", mStore.read(ctx, first));
        assertEquals( "Beta", mStore.read(ctx, second));
    }

    @Test public void persistentWriting() throws Exception {
        Storage<RID,String> mStore = new Storage<RID,String>("test",dataDir);
        RID first = new RID(1);
        RID second = new RID(2);
        Transaction ctx1 = new Transaction();
        Transaction ctx2 = new Transaction();

        mStore.write(ctx1, first, "Alpha");
        mStore.write(ctx1, second, "Beta");

        // ctx2 is never committed
        mStore.write(ctx2, first, "Gamma");
        assertEquals( "Alpha", mStore.read(ctx1, first));

        mStore.prepare(ctx1);
        mStore.commit(ctx1);
        // hard abort goes here
        // reinit storage verify we can read
        
        mStore = new Storage<RID,String>("test",dataDir);
        mStore.readFromDisk();
        Transaction ctx = new Transaction();
        assertEquals( "Alpha", mStore.read(ctx, first));
        assertEquals( "Beta", mStore.read(ctx, second));
    }

    @Test public void consistentReads() throws Exception {
        Storage<RID,String> mStore = new Storage<RID,String>("test",dataDir);
        RID first = new RID(1);
        RID second = new RID(2);
        Transaction ctx1 = new Transaction();
        Transaction ctx2 = new Transaction();
        Transaction ctx3 = new Transaction();

        mStore.write(ctx1, first, "Alpha");
        mStore.prepare(ctx1);
        mStore.commit(ctx1);

        assertEquals( "Alpha", mStore.read(ctx3, first));
        mStore.write(ctx2, first, "Gamma");
        mStore.prepare(ctx2);
        mStore.commit(ctx2);
        assertEquals( "Alpha", mStore.read(ctx3, first));

        Transaction ctx = new Transaction();
        assertEquals( "Gamma", mStore.read(ctx, first));
    }

    @After public void tearDown() {
        dataDir.delete();
    }
}

