import java.util.UUID;

import org.junit.Test;

import tp.*;
import tp.LM.*;
import tp.impl.*;

public class LockTest {
    private LM testLM;
    
    public LockTest() {
        this.testLM = new MyLM();
    }
    
    @Test
    public void singleReadLock() throws DeadLockException, InterruptedException {
        Transaction context = new Transaction(UUID.randomUUID().toString());
        Lockable lockId = new LockableID("lock1");
        
        testLM.lockForRead(context, lockId);
    }
    
    @Test
    public void singleWriteLock() throws DeadLockException, InterruptedException {
        Transaction context = new Transaction(UUID.randomUUID().toString());
        Lockable lockId = new LockableID("lock1");
        
        testLM.lockForWrite(context, lockId);
    }
    
    @Test
    public void multipleReadLocks() throws DeadLockException, InterruptedException {
        Transaction context1 = new Transaction(UUID.randomUUID().toString());
        Transaction context2 = new Transaction(UUID.randomUUID().toString());
        Lockable lockId = new LockableID("lock1");
        
        testLM.lockForRead(context1, lockId);
        testLM.lockForRead(context2, lockId);
    }
    
    @Test(expected=DeadLockException.class)
    public void blockWhenRequestingReadLock() throws DeadLockException, InterruptedException {
        Transaction context1 = new Transaction(UUID.randomUUID().toString());
        Transaction context2 = new Transaction(UUID.randomUUID().toString());
        Lockable lockId = new LockableID("lock1");
        
        testLM.setDeadlockTimeout(100);
        
        testLM.lockForWrite(context1, lockId);
        testLM.lockForRead(context2, lockId);
    }
    
    @Test(expected=DeadLockException.class)
    public void blockWhenRequestingWriteLock() throws DeadLockException, InterruptedException {
        Transaction context1 = new Transaction(UUID.randomUUID().toString());
        Transaction context2 = new Transaction(UUID.randomUUID().toString());
        Lockable lockId = new LockableID("lock1");
        
        testLM.setDeadlockTimeout(100);
        
        testLM.lockForWrite(context1, lockId);
        testLM.lockForWrite(context2, lockId);
    }
    
    @Test
    public void convertReadLockToWriteLock() throws DeadLockException, InterruptedException {
        Transaction context = new Transaction(UUID.randomUUID().toString());
        Lockable lockId = new LockableID("lock1");
        
        testLM.lockForRead(context, lockId);
        testLM.lockForWrite(context, lockId);
    }
    
    @Test
    public void convertWriteLockToReadLock() throws DeadLockException, InterruptedException {
        Transaction context = new Transaction(UUID.randomUUID().toString());
        Lockable lockId = new LockableID("lock1");

        testLM.lockForWrite(context, lockId);
        testLM.lockForRead(context, lockId);
    }
    
    @Test(expected=DeadLockException.class)
    public void blockWhenConvertingLock() throws DeadLockException, InterruptedException {
        Transaction context1 = new Transaction(UUID.randomUUID().toString());
        Transaction context2 = new Transaction(UUID.randomUUID().toString());
        Lockable lockId = new LockableID("lock1");
        
        testLM.setDeadlockTimeout(100);
        
        testLM.lockForRead(context1, lockId);
        testLM.lockForRead(context2, lockId);
        
        testLM.lockForWrite(context1, lockId);
    }
    
    @Test(expected=DeadLockException.class)
    public void blockAfterConvertingLock() throws DeadLockException, InterruptedException {
        Transaction context1 = new Transaction(UUID.randomUUID().toString());
        Transaction context2 = new Transaction(UUID.randomUUID().toString());
        Lockable lockId = new LockableID("lock1");
        
        testLM.setDeadlockTimeout(100);
        
        testLM.lockForRead(context1, lockId);
        testLM.lockForWrite(context1, lockId);
        
        testLM.lockForRead(context2, lockId);
    }
}
