package tp;


/**
 * A problem occurred that caused the transaction to abort.
 * A deadlock might be the problem, or perhaps a device or communication 
 * failure caused this operation to abort the transaction.
 */
public class TransactionAbortedException extends Exception 
{
    private static final long serialVersionUID = 846625016067800434L;

    public TransactionAbortedException(Transaction Xid, String msg) 
    {
        super("The transaction " + Xid + " aborted:" + msg);
    }
}
