package tp;

import java.io.IOException;
import java.io.Serializable;

/**
 * The class represents a resource identifier.
 * 
 * There are three types of resource.
 * <ul>
 * <li>Flight</li>
 * <li>Room</li>
 * <li>Car</li>
 * </ul>
 * 
 * The external string representation ({@link #toString()}) of this identifier is following.
 * 
 * <p>
 * <i>prefix ':' identifier</i>
 * </p>
 * 
 * Valid prefix is 'F', 'R', 'C', which correspond to Flight, Room, and Car.
 * {@link #getName()} returns the identifier part.
 */
public class RID implements Serializable, Comparable<RID>, Lockable {
    private static final long serialVersionUID = 7717150775758337149L;
    
    /**
     * resource type
     */
    public static enum Type {
        INVALID,
        FLIGHT,
        ROOM,
        CAR;
        
        private static String _prefixes = "!FRC";
        
        public char prefix() {
            return _prefixes.charAt(ordinal());
        }
        
        public static Type getInstance(String s) {
            int ch = s.charAt(0);
            int ord = _prefixes.indexOf(ch);
            return Type.class.getEnumConstants()[ord];
        }
    }

    public static final int MAX_NAME_LENGTH = 24;
    
    protected Type   type;
    protected String name;

    public RID() {
        type = Type.INVALID;
        name = "";
    }
    
    public RID(Type type,String name) {
        if ( type == Type.INVALID || name == null || name.length() > MAX_NAME_LENGTH ) {
            throw new IllegalArgumentException();
        }
        this.type = type;
        this.name = name;
    }

    /**
     * create identifier with flight number
     * @param flight the flight number
     */
    public RID(int flight) {
        this.type = Type.FLIGHT;
        this.name = Integer.toString(flight);
    }

    /**
     * get type of resource id
     * @return type value of current identifier
     */
    public Type getType() { return type; }
    
    /**
     * @return name of resource
     */
    public String getName() { return name; }

    @Override
    public int hashCode() {
        return name.hashCode() + type.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o instanceof RID ) {
            RID other = (RID)o;
            return type == other.type && name.equals(other.name);
        }
        return false;
    }

    @Override
    public int compareTo(RID o) {
        int cmp = type.compareTo(o.type);
        return cmp == 0 ? name.compareTo(o.name) : cmp;
    }

    @Override
    public String toString() {
        return type.prefix() + ":" + name;
    }
    
    /**
     * convenient method to create flight resource identifier
     * @param flight flight number
     * @return flight resource identifier
     */
    public static RID forFlight(int flight) {
        return new RID(flight);
    }

    /**
     * convenient method to create flight resource identifier
     * @param flight flight number in string
     * @return flight resource identifier
     */
    public static RID forFlight(String flight) {
        return new RID(Type.FLIGHT,flight);
    }

    /**
     * convenient method to create car resource identifier
     * @param loc location
     * @return car resource identifier
     */
    public static RID forCar(String loc) {
        return new RID(Type.CAR,loc);
    }

    /**
     * convenient method to create room resource identifier
     * @param loc location
     * @return room resource identifier
     */
    public static RID forRoom(String loc) {
        return new RID(Type.ROOM,loc);
    }
    
    /**
     * parse external string representation
     * @param s result of {@link #toString()}
     * @return newly created resource identifier
     */
    public static RID parse(String s) {
        Type t = Type.getInstance(s);
        String id = s.substring(2);
        return new RID(t,id);
    }
    
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.writeUTF(this.toString());
    }
    
    private void readObject(java.io.ObjectInputStream in)
    throws IOException, ClassNotFoundException {
        String s = in.readUTF();
        type = Type.getInstance(s);
        name = s.substring(2);
    }
}
