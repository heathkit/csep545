package tp;

import java.io.Serializable;
import java.util.UUID;

/**
 * unique transaction identifier
 */
public class Transaction implements Serializable, Comparable<Transaction> {
    private static final long serialVersionUID = -880572407004669272L;
    
    private UUID id;

    public Transaction() {
        id = UUID.randomUUID();
    }
    
    public Transaction(String s) {
        id = UUID.fromString(s);
    }

    public Transaction(UUID xid) {
        id = xid;
    }

    @Override
    public boolean equals(Object o) {
        return id.equals(o instanceof Transaction ? ((Transaction)o).id : o);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Transaction: "+id;
    }

    @Override
    public int compareTo(Transaction other) {
        return id.compareTo(other.id);
    }
}
