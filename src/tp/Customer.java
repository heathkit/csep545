package tp;

import java.io.Serializable;
import java.util.UUID;

/**
 * unique customer identifier
 */
public class Customer implements Comparable<Customer>, Lockable, Serializable {
    private static final long serialVersionUID = 4174836339997089550L;
    
    private UUID id;

    public Customer() {
        id = UUID.randomUUID();
    }
    
    public Customer(String s) {
        id = UUID.fromString(s);
    }

    public Customer(UUID xid) {
        id = xid;
    }

    @Override
    public boolean equals(Object o) {
        return id.equals(o instanceof Customer ? ((Customer)o).id : o);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Customer: "+id;
    }

    @Override
    public int compareTo(Customer other) {
        return id.compareTo(other.id);
    }
}
