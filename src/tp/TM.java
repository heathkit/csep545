package tp;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Transaction Manager Interface
 *
 * The default set of APIs except {@link #register(String)} 
 * is exposed to {@link WC}.
 *
 * <b>Please define extra APIs as necessary to implement two-phase commit.</b>
 */
//public interface TM extends Remote {
public interface TM extends Remote {

    /**
     * start a new transaction
     * @return newly generated unique transaction context
     * @see tp.WC#start()
     */
    Transaction start() throws RemoteException;

    /**
     * commit transaction
     * @param xid transaction identifier
     * @see tp.WC#commit(Transaction)
     */
    void commit(Transaction xid)
    throws InvalidTransactionException, TransactionAbortedException, RemoteException;

    /**
     * Query about the decision of the given transaction.  If it was committed, return true.
     * If it was aborted or we just don't know, return false
     */
    boolean isTransactionCommitted(Transaction xid)
    throws InvalidTransactionException, TransactionAbortedException, RemoteException;

    /**
     * abort transaction
     * @param xid transaction identifier
     * @see tp.WC#abort(Transaction)
     */
    void abort(Transaction xid)
    throws RemoteException;

    /**
     * enlist the RM as a member of this transaction
     * @param xid transaction identifier
     * @param enlistingRM RM to add to transaction identified by xid
     * @return <code>true</code> when enlisted. <code>false</code> otherwise.
     * @see tp.RM#getName()
     */
    boolean enlist(Transaction xid, String enlistingRM)
    throws TransactionAbortedException, 
          InvalidTransactionException, RemoteException;
    
    /**
     * A RM of which name is <code>rm</code> register itself 
     * so that later TM could coordinate for two-phase commit.
     * On invocation, the TM contact registry to setup RPC (RMI for Java).
     * @param rm name of newly started resource manager
     * @return transaction that this RM prepared to commit but couldn't. <code>null</code> otherwise.
     * @see tp.RM#getName()
     */
    void register(String rm) throws RemoteException;
}
