package tp;

/**
 * <p>
 * A generic ID that encapsulate a string.
 * You can compose two identifiers to create a hierarchy.
 * The internal representation is just a string. The hierarchy
 * is represented as a standard path (i.e., '/' delimited string 
 * except the last component of the path).
 * </p>
 * 
 * <p>For example, you can use this class to represent a reservation record
 * by constructing a hierarchy of {@link Customer} and {@link RID}
 * as follow.</p>
 * 
 * <pre>
 * Customer customer;
 * RID resource;
 * LockableID id = new LockableID(customer,resource);
 * </pre>
 *
 * <p><b>Please feel free to modify this class.</b></p>
 */
public class LockableID implements Comparable<LockableID>, Lockable {
    private String id;
    
    public LockableID(String id) {
        this.id = id;
    }
    
    public LockableID(Lockable item) {
        this.id = item.toString();
    }

    public LockableID(String prefix,Lockable item) {
        this.id = prefix + "/" + item.toString();
    }
    
    public LockableID(Lockable prefix,Lockable item) {
        this.id = prefix.toString() + "/" + item.toString();
    }
    
    public LockableID(Lockable prefix,String item) {
        this.id = prefix.toString() + "/" + item;
    }
    
    /**
     * Construct a composite ID using this id as prefix.
     * @param other id to compose
     * @return a composite ID of this '/' other
     */
    public LockableID concat(Lockable other) {
        return new LockableID(this,other);
    }
    
    /**
     * check whether <code>this</code> object is prefix of the <code>other</code>.
     * @param other tested id
     * @return <code>true</code> if this object is prefix of the other.
     */
    public boolean isPrefixOf(LockableID other) {
        return id.length() <= other.id.length()
                && other.id.charAt(id.length()) == '/'
                && other.id.startsWith(id);
    }

    /**
     * Get the prefix of this id.
     * @return the prefix of this id. <code>null</code> if this id is a root.
     */
    public LockableID getPrefix() {
        int pos = id.lastIndexOf('/');
        if ( pos < 0 ) return null;
        return new LockableID(id.substring(0,pos));
    }

    /**
     * Get the last component of this id.
     * @return the last component of the path represented by this id.
     */
    public String getName() {
        int pos = id.lastIndexOf('/');
        return pos < 0 ? id : id.substring(pos+1);
    }
    
    @Override
    public int hashCode() {
        return id.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o instanceof LockableID ) {
            return id.equals( ((LockableID)o).id );
        }
        return false;
    }
    
    @Override
    public String toString() { return id; }

    @Override
    public int compareTo(LockableID o) {
        // we are very naive in comparison
        return id.compareTo(o.id);
    }
}
