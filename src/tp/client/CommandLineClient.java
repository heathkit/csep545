package tp.client;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;

import tp.Customer;
import tp.Transaction;
import tp.WC;
import tp.impl.CSEP545Service;
import tp.impl.ServiceID;
import tp.impl.CSEP545Service.Type;

/**
 * default command line client for the project.
 * 
 * <table border="1">
 * <tr>
 * <th>Command</th><th>Description</th>
 * </tr>
 * <tr><th colspan="2">Interpreter and Service</th></tr>
 * <tr><td>print</td><td>Print current transaction and customer information</td></tr>
 * <tr><td>exit or quit</td><td>Terminate the client.</td></tr>
 * <tr><td>refresh</td>
 * <td>Tell all managers to refresh remote references.</td></tr>
 * <tr><td>kill (WC|RM|TM) [name]</td>
 * <td>Kill specified manager.</td></tr>
 * <tr><td>shutdown (WC|RM|TM) [name]</td>
 * <td>Gracefully shutdown specified manager.</td></tr>
 * <tr><th colspan="2">Transactions and Customer</th></tr>
 * <tr><td>begin</td><td>begin transaction</td></tr>
 * <tr><td>commit</td><td>commit transaction</td></tr>
 * <tr><td>abort</td><td>abort transaction</td></tr>
 * <tr><td>new [UUID]</td><td>Set current customer id. If UUID is not given, a random customer id is created.</td></tr>
 * <tr><th colspan="2">Managing Resources</th></tr>
 * <tr><td>add (car|seat|room) qty price</td><td>add resources</td></tr>
 * <tr><td>delete (car|seat|room) qty</td><td>delete resources</td></tr>
 * <tr><td>delete flight</td><td>cancel flight and all associate reservations.</td></tr>
 * <tr><td>query (car|seat|room) loc</td><td>query available resource at loc</td></tr>
 * <tr><td>price (car|seat|room) loc</td><td>query price of resource at loc</td></tr>
 * <tr><td>list (car|seat|room)</td><td>list registered resources.</td></tr>
 * <tr><th colspan="2">Managing Reservations</th></tr>
 * <tr><td>reserve f1 ... fN loc bookCar bookRoom</td>
 * <td>make a reservation for current customer.
 * f1 ... fN are flights.
 * bookCar and bookRoom are boolean values.</td></tr>
 * <tr><td>cancel [customer]</td><td>cancel reservation for given or current customer</td></tr>
 * <tr><td>query itinerary [customer]</td><td>query itinerary of given or current customer</td></tr>
 * <tr><td>price itinerary [customer]</td><td>query price of itinerary of given or current customer</td></tr>
 * <tr><td>list customer</td><td>list registered customers.</td></tr>
 * </table>
 */
public class CommandLineClient {
    private final Registry registry;
    private WC myWC;
    private Customer customer;
    private Transaction tx;

    public CommandLineClient(Registry r,WC wc) {
        registry = r;
        myWC = wc;
    }
    
    private boolean parseBoolean(String s) {
        return "yYtT".indexOf(s.charAt(0)) >= 0;
    }
    
    private void run(String line) throws Exception {
        String[] cmds = line.split("\\s+");
        if ( cmds.length == 0 ) return;
        
        String command = cmds[0];
        
        try {
            if ( "begin".equalsIgnoreCase(command) ) {
                if ( customer == null ) {
                    throw new IllegalStateException("no customer present");
                }
                if ( tx != null ) {
                    throw new IllegalStateException("transaction "+tx+" is active");
                }
                tx = myWC.start();
            } else if ( "commit".equalsIgnoreCase(command) ) {
                if ( tx == null ) {
                    throw new IllegalStateException("no transaction is active");
                }
                myWC.commit(tx);
                tx = null;
            } else if ( "abort".equalsIgnoreCase(command) ) {
                if ( tx != null ) {
                    myWC.abort(tx);
                }
                tx = null;
            } else if ( "cancel".equalsIgnoreCase(command) ) {
                if ( tx == null ) {
                    throw new IllegalArgumentException("no active transaction");
                }
                Customer c = cmds.length == 2 ? new Customer(cmds[1]) : customer;
                if ( myWC.cancelItinerary(tx, c) ) {
                    System.out.println("Itinierary for customer "+c+" has canceled");
                } else {
                    System.out.println("Failed to cancel itinerary for customer "+c);
                }
            } else if ( "new".equalsIgnoreCase(command) ) {
                if ( tx != null ) {
                    throw new IllegalArgumentException("A transaction is active. Please commit or abort first.");
                }
                if ( customer != null ) {
                    System.out.println("Old customer = "+customer);
                }
                if ( cmds.length == 2 ) {
                    customer = new Customer(cmds[1]);
                } else {
                    customer = new Customer();
                }
                System.out.println("current customer = "+customer);
            } else if ( "print".equalsIgnoreCase(command) ) {
                if ( tx != null ) {
                    System.out.println("Current transaction = "+tx);
                }
                if ( customer != null ) {
                    System.out.println("Current customer = "+customer);
                }
            } else if ( "add".equalsIgnoreCase(command) ) {
                if ( tx == null ) {
                    throw new IllegalArgumentException("no active transaction");
                }
                String target = cmds[1];
                String loc = cmds[2];
                int    num = Integer.parseInt(cmds[3]);
                int    price = Integer.parseInt(cmds[4]);
                if ( "car".equalsIgnoreCase(target) ) {
                    myWC.addCars(tx, loc, num, price);
                } else if ( "seat".equalsIgnoreCase(target) ) {
                    myWC.addSeats(tx, loc, num, price);
                } else if ( "room".equalsIgnoreCase(target) ) {
                    myWC.addRooms(tx, loc, num, price);
                } else {
                    throw new IllegalArgumentException("usage: add (seat|car|room) loc qty price");
                }
            } else if ( "del".equalsIgnoreCase(command) || "delete".equalsIgnoreCase(command) ) {
                if ( tx == null ) {
                    throw new IllegalArgumentException("no active transaction");
                }
                String target = cmds[1];
                String loc = cmds[2];
                if ( "flight".equalsIgnoreCase(target) ) {
                    myWC.deleteFlight(tx, loc);
                } else {
                    int    num = Integer.parseInt(cmds[3]);
                    if ( "car".equalsIgnoreCase(target) ) {
                        myWC.deleteCars(tx, loc, num);
                    } else if ( "seat".equalsIgnoreCase(target) ) {
                        myWC.deleteSeats(tx, loc, num);
                    } else if ( "room".equalsIgnoreCase(target) ) {
                        myWC.deleteRooms(tx, loc, num);
                    } else {
                        throw new IllegalArgumentException("usage: "+command+" (seat|car|room) qty | flight");
                    }
                }
            } else if ( "query".equalsIgnoreCase(command) ) {
                if ( tx == null ) {
                    throw new IllegalArgumentException("no active transaction");
                }
                String target = cmds[1];
                if ( "itinerary".equalsIgnoreCase(target) || "i".equalsIgnoreCase(target) ) {
                    Customer c = customer;
                    if ( cmds.length == 3 ) {
                        c = new Customer(cmds[2]);
                    } else if ( c == null ) {
                        throw new IllegalArgumentException("usage: query (itinerary|i) [customer]");
                    }
                    String result = myWC.queryItinerary(tx, c);
                    System.out.println("Itinerary for customer "+c);
                    System.out.println(result);
                } else {
                    String loc = cmds[2];
                    int avail = 0;
                    if ( "car".equalsIgnoreCase(target) || "c".equalsIgnoreCase(target) ) {
                        avail = myWC.queryCar(tx, loc);
                    } else if ( "flight".equalsIgnoreCase(target) || "f".equalsIgnoreCase(target)) {
                        avail = myWC.queryFlight(tx, loc);
                    } else if ( "room".equalsIgnoreCase(target) || "r".equalsIgnoreCase(target)) {
                        avail = myWC.queryRoom(tx, loc);
                    } else {
                        throw new IllegalArgumentException("usage: query (car|flight|room) loc");
                    }
                    System.out.println(avail + " " + target + " are available at " + loc);
                }
            } else if ( "price".equalsIgnoreCase(command) ) {
                if ( tx == null ) {
                    throw new IllegalArgumentException("no active transaction");
                }
                String target = cmds[1];
                if ( "itinerary".equalsIgnoreCase(target) || "i".equalsIgnoreCase(target) ) {
                    Customer c = customer;
                    if ( cmds.length == 3 ) {
                        c = new Customer(cmds[2]);
                    } else if ( c == null ) {
                        throw new IllegalArgumentException("usage: price (itinerary|i) [customer]");
                    }
                    int total = myWC.queryItineraryPrice(tx, c);
                    System.out.println("Total price of itinerary for customer "+c+" = "+total);
                } else {
                    String loc = cmds[2];
                    int avail = 0;
                    if ( "car".equalsIgnoreCase(target) || "c".equalsIgnoreCase(target) ) {
                        avail = myWC.queryCarPrice(tx, loc);
                    } else if ( "flight".equalsIgnoreCase(target) || "f".equalsIgnoreCase(target)) {
                        avail = myWC.queryFlightPrice(tx, loc);
                    } else if ( "room".equalsIgnoreCase(target) || "r".equalsIgnoreCase(target)) {
                        avail = myWC.queryRoomPrice(tx, loc);
                    } else {
                        throw new IllegalArgumentException("usage: price (car|flight|room) loc");
                    }
                    System.out.println("price to reserve "+target+" at "+loc+" = "+avail);
                }
            } else if ( "list".equalsIgnoreCase(command) ) {
                if ( tx == null ) {
                    throw new IllegalArgumentException("no active transaction");
                }
                String target = cmds[1];
                if ("customer".equalsIgnoreCase(target) ) {
                    Customer[] customers = myWC.listCustomers(tx);
                    System.out.println(customers.length + " customers exist");
                    for ( Customer c : customers ) {
                        System.out.println(c);
                    }
                } else {
                    String[] result = new String[0];
                    if ( "car".equalsIgnoreCase(target) ) {
                        result = myWC.listCars(tx);
                    } else if ("flight".equalsIgnoreCase(target) ) {
                        result = myWC.listFlights(tx);
                    } else if ("room".equalsIgnoreCase(target) ) {
                        result = myWC.listRooms(tx);
                    } else {
                        throw new IllegalArgumentException("usage: list (car|flight|room|customer)");
                    }

                    System.out.println(result.length + " items exist");
                    for ( String e : result ) {
                        System.out.println(e);
                    }
                }
            } else if ( "reserve".equalsIgnoreCase(command) ) {
                if ( tx == null ) {
                    throw new IllegalArgumentException("no active transaction");
                }
                if ( cmds.length < 4 ) {
                    throw new IllegalArgumentException("usage: reserve [flight1 ... flightN] loc bookCar bookRoom");
                }
                if ( customer == null ) {
                    throw new IllegalArgumentException("no customer was set.");
                }
                
                String[] flights = Arrays.copyOfRange(cmds, 1, cmds.length-3);
                String loc = cmds[cmds.length-3];
                boolean bcar = parseBoolean(cmds[cmds.length-2]);
                boolean broom = parseBoolean(cmds[cmds.length-1]);
                
                if ( myWC.reserveItinerary(tx, customer, flights, loc, bcar, broom) ) {
                    System.out.println("reserved the itinerary for customer "+customer);
                } else {
                    System.out.println("Failed to reserve the itinerary for customer "+customer);
                }
            } else if ( "exit".equalsIgnoreCase(command) || "quit".equalsIgnoreCase(command) ) {
                try {
                    if ( tx != null ) myWC.abort(tx);
                } catch ( Exception ignore ) {}
                System.exit(0);
            } else if ( "kill".equalsIgnoreCase(command) ) {
                if ( cmds.length < 2 ) 
                    throw new IllegalArgumentException("usage: kill (RM|TM|WC) [name]");
                CSEP545Service.Type type = CSEP545Service.Type.getInstance(cmds[1]);
                ServiceID svc = cmds.length == 3 ? new ServiceID(type,cmds[2]) : new ServiceID(type);
                CSEP545Service service = (CSEP545Service) registry.lookup(svc.toString());
                service.kill();
            } else if ( "shutdown".equalsIgnoreCase(command) ) {
                if ( cmds.length < 2 ) 
                    throw new IllegalArgumentException("usage: shutdown (RM|TM|WC) [name]");
                CSEP545Service.Type type = CSEP545Service.Type.getInstance(cmds[1]);
                ServiceID svc = cmds.length == 3 ? new ServiceID(type,cmds[2]) : new ServiceID(type);
                CSEP545Service service = (CSEP545Service) registry.lookup(svc.toString());
                service.shutdown();
            } else if ( "refresh".equalsIgnoreCase(command) ) {
                String[] services = registry.list();
                for ( String svcName : services ) {
                    ServiceID svcid = ServiceID.parse(svcName);
                    if ( svcid.getType() == Type.INVALID ) continue;
                    CSEP545Service service = (CSEP545Service) registry.lookup(svcName);
                    service.refresh();
                }
                // refresh reference to WC
                myWC = (WC) registry.lookup(ServiceID.forWC().toString());
            } else {
                throw new IllegalArgumentException("unknown command: "+command);
            }
        } catch ( IllegalArgumentException x ) {
            System.out.println(x.getMessage());
        } catch ( Exception x ) {
            x.printStackTrace();
        }
    }
    
    private static void printUsage() {
        System.err.println("Usage: CommandLineClient " +
                " [--rmiHost host]" +
                " [--rmiPort port]" +
                " [-c|--code uri]" +
                " [-i|--in file]" +
                " ...");
        System.exit(-1);
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        int argc = 0;
        // default RMI registry connection information
        String rmiHost = "127.0.0.1";
        int rmiPort = 1099;
        String codeBase = null;
        String inFile = null;
        
        for ( ; argc < args.length; ++argc ) {
            String arg = args[argc];
            if ( arg.charAt(0) != '-' ) break;
            
            if ( "--rmiHost".equals(arg) ) {
                rmiHost = args[++argc];
            } else if ( "--rmiPort".equals(arg) ) {
                rmiPort = Integer.parseInt(args[++argc]);
            } else if ( "-c".equals(arg) || "--code".equals(arg) ) {
                codeBase = args[++argc];
            } else if ( "-i".equals(arg) || "--in".equals(arg) ) {
                inFile = args[++argc];
            } else {
                printUsage();
            }
        }
        
        System.out.println("RMI registry = "+rmiHost+":"+rmiPort);
        if ( codeBase == null ) {
            codeBase = System.getProperty("java.rmi.server.codebase");
            if ( codeBase == null || codeBase.length() == 0 ) {
                codeBase = "http://127.0.0.1:8080/csep545.jar";
            }
        }
        
        System.setProperty("java.rmi.server.codebase", codeBase);
        System.out.println("RMI server codebase = "+codeBase);
        
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        
        Registry registry = LocateRegistry.getRegistry(rmiHost,rmiPort);
        WC myWC = (WC) registry.lookup(ServiceID.forWC().toString());
        
        CommandLineClient client = new CommandLineClient(registry,myWC);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inFile == null ? System.in : new FileInputStream(inFile)));
        try {
            String line;
            while ( (line = reader.readLine()) != null ) {
                line = line.trim();
                if ( line.length() > 0 ) {
                    client.run(line);
                }
            }
        } finally {
            if ( reader != null ) try { reader.close(); } catch ( Exception ignore ) {}
            try { client.run("exit"); } catch ( Exception ignore ) {}
        }
    }
}
