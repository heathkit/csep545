package tp;

import java.util.Set;

/**
 * Lock Manager interface
 */
public interface LM {
   enum LockType {
       UNDEF,
       READ,
       WRITE;
       
       private static final boolean[/* request */][/* current */] COMPATIBILITY = {
       //   UNDEF, READ, WRITE
           { true, false, false }, // request: UNDEF
           { true, true,  false }, // request: READ
           { true, false, false }  // request: WRITE
       };
       
       private static final boolean[/* this */][/* other */] SUBSUME = {
       //   UNDEF, READ, WRITE
           { true, false, false }, // this: UNDEF
           { true, true,  false }, // this: READ
           { true, true,  true }  // this: WRITE
       };
       
       /**
        * check whether current lock type is compatible with this lock type
        * @param current type of lock currently held 
        * @return <code>true</code> if the two locks are compatible.
        */
       public boolean isCompatible(LockType current) {
           return COMPATIBILITY[ordinal()][current.ordinal()];
       }
       
       /**
        * check whether current lock type conflicts to this lock type
        * @param current type of lock currently held 
        * @return <code>true</code> if the two locks conflicts.
        */
       public boolean conflicts(LockType current) {
          return !COMPATIBILITY[ordinal()][current.ordinal()];
       }
       
       /**
        * check whether this lock type subsumes (is stronger) other lock type
        * @param other
        * @return <code>true</code> if this lock type is stronger than other
        */
       public boolean subsume(LockType other) {
           return SUBSUME[ordinal()][other.ordinal()];
       }
   }
   
   /**
    * A transaction is considered in deadlock if it waits for lock more than 10 seconds.
    * @see #setDeadlockTimeout(long)
    */
   public static final long DEFAULT_DEADLOCK_TIMEOUT = 10000;

   /**
    * obtain a read lock for given resource
    * @param context transaction identifier
    * @param resource lockable resource identifier
    * @throws DeadLockException deadlock detected by either timeout or explicit deadlock detection algorithm
    * @throws InterruptedException thread has been interrupted by coordination thread
    */
   public void lockForRead(Transaction context, Lockable resource) throws DeadLockException, InterruptedException;
   /**
    * obtain a write lock for given resource
    * @param context transaction identifier
    * @param resource lockable resource identifier
    * @throws DeadLockException deadlock detected by either timeout or explicit deadlock detection algorithm
    * @throws InterruptedException thread has been interrupted by coordination thread
    */
   public void lockForWrite(Transaction context, Lockable resource) throws DeadLockException, InterruptedException;
   /**
    * unlock all locks held by the transaction
    * @param context transaction identifier
    */
   public void unlockAll(Transaction context);

   /**
    * Set deadlock timeout in millisecond. The default value is 10 seconds.
    * Use this interface for testing concurrency control.
    * @param ms timeout value in millisecond
    */
   public void setDeadlockTimeout(long ms);
   
   public class DeadLockException extends Exception {
       private static final long serialVersionUID = 3652716483036646239L;

       private Transaction xid;
       
       public DeadLockException(Transaction cause,String msg) {
           super(msg);
           this.xid = cause;
       }
       
       public Transaction getTransaction() {
           return xid;
       }
   }
}
