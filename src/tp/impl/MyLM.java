package tp.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import tp.LM;
import tp.Lockable;
import tp.Transaction;

/**
 * Your Lock Manager
 * 
 * By default, it supports timeout based deadlock detection.
 * Please refer comments (<b>TODO</b>) in the code when implementing <i>lock conversion</i>
 * and <i>deadlock detection</i> using wait-for graph.
 */
public class MyLM implements LM {
    /**
     * A lock request captures all contextual information
     * including transaction, resource, thread, and timings.
     * This structure is instantiated only when the requesting
     * transaction has to be blocked.
     */
    class LockRequest {
        /**
         * requesting transaction
         */
        final Transaction xid;
        /**
         * target resource
         */
        final Lockable resource;
        /**
         * the time when the request is created (or started to wait)
         */
        final long waitSince;
        /**
         * requesting lock type
         */
        final LockType request;
        /**
         * requesting thread
         */
        final Thread thread;
        
        LockRequest(Transaction xid,Lockable rs,LockType req) {
            this.xid = xid;
            this.resource = rs;
            this.waitSince = System.currentTimeMillis();
            this.request = req;
            thread = Thread.currentThread();
        }
        
        public Transaction getTransaction() { return xid; }
        public Lockable getResource() { return resource; }
        public LockType getLockType() { return request; }
        public Thread getThread() { return thread; }
        public long getRequestTime() { return waitSince; }
        
        /**
         * check whether this request has been timed out
         * @param now the current timestamp in milliseconds
         * @return <code>true</code> if waited too long
         */
        public boolean hasTimeouted(long now) {
            return (now - waitSince) > deadlockTimeout;
        }
        
        public boolean hasTimeouted() {
            return hasTimeouted(System.currentTimeMillis());
        }
        
        /**
         * get remaining timeout value if interupted accidentally.
         * @param now the current timestamp in milliseconds
         * @return remaining timeout in milliseconds
         */
        public long getTimeout(long now) {
            return deadlockTimeout - (now - waitSince);
        }
        
        public long getTimeout() {
            return getTimeout(System.currentTimeMillis());
        }
        
        @Override
        public int hashCode() { return xid.hashCode(); }

        @Override
        public boolean equals(Object o) {
            if ( this == o ) return true;
            if ( o instanceof LockRequest ) {
                LockRequest other = (LockRequest)o;
                return xid.equals(other.xid) &&  resource.equals(other.resource);
            }
            return false;
        }
        
        @Override
        public String toString() {
            return request+" lock request "+xid+" for "+resource+" at "+waitSince;
        }
    }
    
    /**
     * {@link LockInfo} is defined per resource and
     * tracks holders and waiters of the lock.
     * 
     * For your convenience, all public methods of this class
     * is synchronized; you can safely manipulate the
     * member data structures to implement advanced features
     * such as lock scheduling, deadlock detection.
     */
    class LockInfo {
        /**
         * resource associated with this lock
         */
        final Lockable resource;
        /**
         * the type of lock currently held on {@link resource}
         */
        LockType locked;
        /**
         * set of transactions that holding the lock
         */
        Set<Transaction> owners;
        /**
         * list of transactions that waiting for lock
         */
        LinkedList<LockRequest> waiters;
        
        LockInfo(Lockable resource) {
            this.resource = resource;
            locked = LockType.UNDEF;
            owners = new LinkedHashSet<Transaction>();
            waiters = new LinkedList<LockRequest>();
        }
        
        /**
         * test whether the transaction owns a lock on this resource
         * @param context transaction identifier
         * @return type of lock if the transaction holds the lock.
         * {@link TP.LM.LockType.UNDEF} if not.
         */
        private LockType isOwnedBy(Transaction context) {
            return owners.contains(context) ? locked : LockType.UNDEF;
        }
     
        /**
         * try to acquire lock on this resource. This call is non-blocking.
         * @param context transaction
         * @param request type of lock
         * @return <code>true</code> when it successfully acquire a lock. otherwise, <code>false</code>
         */
        private boolean tryLock(Transaction context,LockType request) {
            boolean granted = false;
            if ( ! request.conflicts(locked) ) {
                owners.add(context);
                locked = request;
                granted = true;
            }
            return granted;
        }
        
        /**
         * request lock of associated resource
         * @param context requesting transaction
         * @param request type of lock
         */
        public synchronized void lock(Transaction context,LockType request)
        throws DeadLockException, InterruptedException {
            if (request == LockType.UNDEF) {
                // don't handle lock requests which are undefined
                return;
            }
            
            // Create a new LockRequest
            LockRequest req = new LockRequest(context,resource,request);
            
            LockType hold = isOwnedBy(context);
            if ( hold == LockType.UNDEF ) {
                // we don't hold the lock
            } else if ( hold == LockType.WRITE || hold == request ) {
                // if the transaction owns the write lock 
                // or requests the same type of lock again,
                // it is automatically granted
                return;
            } else { // hold == LockType.READ and request == LockType.WRITE
                // unlock the read lock that is being held
                // first, so that this thread
                // can try the WRITE lock request in
                // tryLock in the next step
                unlockWithoutSchedulingNextWaiter(context);
            }
        
            // try to lock
//            System.out.println(context+" req="+request+" locked="+locked+" "+request.conflicts(locked)+" "+this);
            if ( ! tryLock(context,request) ) {
                // retry. if the transaction can not obtain the lock by timeout
                // deadlock exception will be thrown
                waiters.add(req);
                long now = req.getRequestTime();
                long to = req.getTimeout(now);
                try {
                    // loop until we obtain the lock or timeout (notified by deadlock exception)
                    do {
                        wait(to);
                        // woke up by either timeout or notification

                        now = System.currentTimeMillis();
                        if ( req.hasTimeouted(now) || (to = req.getTimeout(now)) <= 0 ) {
                            // and it's deadlocked.
                            throw new DeadLockException(context,"deadlock "+context+" on "+resource);
                        }
                        // try to lock again.
                    } while ( ! tryLock(context,request) );
                } finally {
                    // leaving this block means 
                    // 1. the transaction got the requested lock
                    // 2. deadlock exception has been thrown
                    waiters.remove(req);
                }
            }
            // we got the lock by now!
        }

        /**
         * unlock resource held by given transaction but don't notify
         * any threads waiting on this lock
         * @param context transaction holding the lock on this resource
         */
        private void unlockWithoutSchedulingNextWaiter(Transaction context) {
            if ( !owners.remove(context) ) {
                throw new IllegalStateException(context+" tried to release a lock it does not own "+resource);
            }
            
//          System.out.println(context+" unlock "+resource);
            
            // if this is the last transaction holding the lock
            if ( owners.isEmpty() ) {
                // mark the lock as UNDEF
                locked = LockType.UNDEF;
            }
        }
        
        /**
         * unlock resource held by given transaction
         * @param context transaction holding the lock on this resource
         */
        public synchronized void unlock(Transaction context) {
            // unlock the lock being held by this thread
            unlockWithoutSchedulingNextWaiter(context);
            
            // if this is the last transaction holding the lock
            if ( owners.isEmpty() ) {
                // TODO you can implement lock scheduling here
                // if there are threads waiting for the lock
                if ( waiters.size() > 0 ) {
                    // wake up the waiters
                    notifyAll();
                }
            }
        }
    }

    private long deadlockTimeout = DEFAULT_DEADLOCK_TIMEOUT;

    /**
     * locks (resources) held by transaction
     */
    Map<Transaction,Set<Lockable>> lockTable;
    
    /**
     * resource to lock information mapping
     */
    Map<Lockable,LockInfo> resources;
    
    public MyLM() {
        lockTable = new HashMap<Transaction,Set<Lockable>>();
        resources = new HashMap<Lockable,LockInfo>();
    }
    
    private void lock(Transaction context,Lockable resource,LockType request)
    throws DeadLockException, InterruptedException {
        LockInfo lockInfo;
        
        // create lock info structure if necessary
        synchronized (resources) {
            lockInfo = resources.get(resource);
            if ( lockInfo == null ) {
                lockInfo = new LockInfo(resource);
                resources.put(resource, lockInfo);
            }
        }
        
        lockInfo.lock(context,request);
        
        // being here, we obtained the lock. register this transaction.
        synchronized (lockTable) {
            Set<Lockable> locked = lockTable.get(context);
            if ( locked == null ) {
                locked = new HashSet<Lockable>(3);
                lockTable.put(context,locked);
            }
            locked.add(resource);
        }
    }
    
    @Override
    public void lockForRead(Transaction context, Lockable resource)
    throws DeadLockException, InterruptedException {
        lock(context,resource,LockType.READ);
    }

    @Override
    public void lockForWrite(Transaction context, Lockable resource)
    throws DeadLockException, InterruptedException {
        lock(context,resource,LockType.WRITE);
    }

    @Override
    public void unlockAll(Transaction context) {
        Set<Lockable> lockedResources;
        synchronized (lockTable) {
            lockedResources = lockTable.remove(context);
        }
        
        if ( lockedResources == null ) {
            // the transaction does not hold any lock
            // on resources.
            return;
        }
        
        for ( Lockable resource : lockedResources ) {
            LockInfo lockInfo;
            synchronized (resources) {
                lockInfo = resources.get(resource);
            }
            
            if ( lockInfo == null ) {
                // error
                throw new IllegalStateException("lock does not exist: "+resource);
            } else {
                lockInfo.unlock(context);
            }
        }
    }

    @Override
    public void setDeadlockTimeout(long ms) {
        deadlockTimeout = ms;
    }
}
