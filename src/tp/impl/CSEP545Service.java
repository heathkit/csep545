package tp.impl;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Common service interface for the project.
 * This interface is <b>specific to Java</b>.
 */
public interface CSEP545Service extends Remote {
    /**
     * type of service
     */
    public static enum Type {
        INVALID,
        /**
         * resource manager
         */
        RM,
        /**
         * workflow controller
         */
        WC,
        /**
         * transaction manager
         */
        TM;

        private static String _prefixes = "!RWT";
        
        public char prefix() {
            return _prefixes.charAt(ordinal());
        }
        
        public static Type getInstance(String s) {
            int ch = s.toUpperCase().charAt(0);
            int ord = _prefixes.indexOf(ch);
            return ord < 0 ? Type.INVALID : Type.class.getEnumConstants()[ord];
        }
        
        public boolean hasType(String s) {
            int ch = s.toUpperCase().charAt(0);
            int ord = _prefixes.indexOf(ch);
            return ordinal() == ord;
        }
    }
    
    /**
     * Name of remote service object
     * @return A string represent remote service object
     * @throws RemoteException
     */
    public ServiceID getServiceID() throws RemoteException;
    
    /**
     * Immediately kill remote service. Useful for automated testing.
     * @throws RemoteException
     */
    public void kill() throws RemoteException;
    
    /**
     * Gracefully shutdown target service.
     * On restart, the service should not need to recover its state.
     * 
     * Please make sure to call {@link java.rmi.registry.Registry#unbind(String)}
     * @see java.rmi.registry.Registry#unbind(String)
     */
    public void shutdown() throws RemoteException;
    
    /**
     * Refresh all remote references used by this service.
     * For easier automated testing recovery from failure.
     */
    public void refresh() throws RemoteException, NotBoundException;
}
