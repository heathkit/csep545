package tp.impl;

import java.io.IOException;
import java.io.Serializable;

import tp.impl.CSEP545Service.Type;

/**
 * The class represents a service for Java project.
 * 
 * There are three types of services.
 * <ul>
 * <li>RM</li>
 * <li>WC</li>
 * <li>TM</li>
 * </ul>
 * 
 * The external string representation ({@link #toString()}) of this identifier is following.
 * 
 * <p>
 * <i>prefix ['-' identifier]</i>
 * </p>
 * 
 * Valid prefix is 'R', 'W', 'T', which correspond to RM, WC, and TM.
 * {@link #getName()} returns the identifier part.
 * 
 * Note that the identifier part is optional.
 */
public final class ServiceID implements Serializable, Comparable<ServiceID> {
    /**
     * 
     */
    private static final long serialVersionUID = 5284092841385973788L;

    public static final int MAX_NAME_LENGTH = 24;
    
    protected Type   type;
    protected String name;
    /**
     * remote address.
     * does not affect identity test. 
     */
    protected String addr;

    public ServiceID() {
        type = Type.INVALID;
        name = "";
    }

    public ServiceID(Type type) {
        this.type = type;
        this.name = "";
    }
    
    public ServiceID(Type type,String name) {
        if ( type == Type.INVALID || name == null || name.length() > MAX_NAME_LENGTH ) {
            throw new IllegalArgumentException();
        }
        this.type = type;
        this.name = name;
    }

    /**
     * get type of service id
     * @return type value of current identifier
     */
    public Type getType() { return type; }
    
    /**
     * @return name of service. An empty string for default service instance.
     */
    public String getName() { return name; }

    @Override
    public int hashCode() {
        return name.hashCode() + type.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o instanceof ServiceID ) {
            ServiceID other = (ServiceID)o;
            return type == other.type && name.equals(other.name);
        }
        return false;
    }

    @Override
    public String toString() {
        return type.prefix() + (name.length() > 0 ? ("-" + name) : "");
    }

    /**
     * service id for default RM instance
     * @return default service id
     */
    public static ServiceID forRM() {
        return new ServiceID(Type.RM);
    }
   
   public static ServiceID forRM(String s) {
        return new ServiceID(Type.RM,s);
    }

   /**
    * service id for default WC instance
    * @return default service id
    */
   public static ServiceID forWC() {
       return new ServiceID(Type.WC);
   }

   public static ServiceID forWC(String s) {
       return new ServiceID(Type.WC,s);
   }

   /**
    * service id for default TM instance
    * @return default service id
    */
   public static ServiceID forTM() {
        return new ServiceID(Type.TM);
    }
    
    /**
     * parse external string representation
     * @param s result of {@link #toString()}
     * @return newly created resource identifier
     */
    public static ServiceID parse(String s) {
        Type t = Type.getInstance(s);
        String id = s.length() > 2 ? s.substring(2) : "";
        return new ServiceID(t,id);
    }
    
    private void writeObject(java.io.ObjectOutputStream out)
    throws IOException {
        out.writeUTF(this.toString());
    }
    
    private void readObject(java.io.ObjectInputStream in)
    throws IOException, ClassNotFoundException {
        String s = in.readUTF();
        type = Type.getInstance(s);
        name = s.length() > 2 ? s.substring(2) : "";
    }

    @Override
    public int compareTo(ServiceID other) {
        int cmp = type.compareTo(other.type);
        return cmp == 0 ? name.compareTo(other.name) : cmp;
    }
}
