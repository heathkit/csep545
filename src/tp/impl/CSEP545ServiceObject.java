package tp.impl;

import java.io.File;
import java.io.IOException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * A stub class to drive project services.
 * 
 * <h3>Bootstrap</h3>
 * 
 * The bootstrap happens in the following order.
 * 
 * <ol>
 * <li>process common command line arguments</li>
 * <li>cleanup storage directory if necessary</li>
 * <li>{@link #init(String[])}</li>
 * <li>{@link #initStorage()} if <i>initializing the storage</i> or {@link #recovery()} if <i>storage</i> exists</li>
 * <li>{@link #startUp()}</li>
 * <li>{@link java.rmi.registry.Registry#rebind(String, Remote)}</li>
 * <li>{@link #readyToServe()}</li>
 * </ol>
 * 
 * <h3>Common arguments</h3>
 * 
 * The following arguments are processed by this class.
 * 
 * <table border="1">
 * <tr>
 * <th>Argument</th><th>Description</th><th>Default</th></tr>
 * <tr>
 * <td><code>-n name</code> or <code>--name name</code></td>
 * <td>the name of service</td>
 * <td>Type of service</td></tr>
 * <tr>
 * <td><code>-p port</code> or <code>--port port</code></td>
 * <td>port number to bind the service</td>
 * <td>anonymous port</td></tr>
 * <tr>
 * <td><code>-d dir</code> or <code>--dir dir</code></td>
 * <td>the data directory where all necessary files are.</td> 
 * <td>'./' {@link ServiceID} '-data'</td></tr>
 * <tr>
 * <td><code>--init</code></td>
 * <td>initialize the data directory. 
 * will wipe out data directory if it exists. 
 * initialize if data directory does not exist,
 * {@link #recovery()} otherwise.</td>
 * <td>Do not force initialization</td></tr>
 * <tr><td><code>--rmiHost host</code></td>
 * <td>host name of RMI registry</td>
 * <td>127.0.0.1</td>
 * </tr>
 * <tr>
 * <td><code>--rmiPort port</code></td>
 * <td>port number of RMI registry.</td>
 * <td>1099</td></tr>
 * <tr>
 * <td><code>-c URI</code> or <code>--code URI</code></td>
 * <td>RMI server code base URL</td>
 * <td>http://127.0.0.1:8080/csep545.jar</td>
 * </tr>
 * </table>
 *
 */
public abstract class CSEP545ServiceObject implements CSEP545Service {
    private static final long serialVersionUID = 6816554567591429196L;

    /**
     * service identifier. must be unique per instance.
     */
    protected ServiceID svcid;
    /**
     * directory to store all data files
     */
    protected File   dir;
    /**
     * reference to RMI registry
     */
    protected transient Registry registry;

    private void setServiceID(ServiceID id) {
        this.svcid = id;
    }

    @Override
    public final void kill() throws RemoteException {
        System.err.println("Kill "+svcid);
        System.exit(-1);
    }

    @Override
    public void shutdown() throws RemoteException {
        System.err.println("Shutdown "+svcid);
        try {
            registry.unbind(svcid.toString());
        } catch (NotBoundException e) {}
        System.exit(0);
    }
    
    @Override
    public final ServiceID getServiceID() { return svcid; }
    
    /**
     * Get root directory where local data files are located.
     * By default, {@link #getServiceID()} '-data' under working directory.
     * @return root directory to place data files
     */
    public final File getDataRoot() {
        return dir;
    }

    /**
     * <b>IMPLEMENT THIS METHOD</b>
     * process command line parameters and setup any parameterized values.
     * For example, setting self destruct counter should be done in this method.
     * @param args command line arguments except common arguments
     */
    protected abstract void init(String[] args) throws Exception;

    /**
     * <b>IMPLEMENT THIS METHOD</b>
     * Initialize necessary data files. Invoked in two cases.
     * <ul>
     * <li>the data directory is newly created.</li>
     * <li><i>--init</i> flag is set in command line.</li>
     * </ul>
     * 
     * For example, you can create database files for {@link tp.RM}
     * and commit log file for {@link tp.TM}
     */
    protected abstract void initStorage() throws Exception;
    
    /**
     * <b>IMPLEMENT THIS METHOD</b>
     * Run recovery algorithm and reconstruct the state of service before failure.
     * This method is invoked when the data directory already exists.
     * Note that if the service has been shutdown cleanly, calling this method
     * should not affect the state.
     */
    protected abstract void recovery() throws Exception;
    
    /**
     * <b>IMPLEMENT THIS METHOD</b>
     * If you use extra threads in your service, start them in this method.
     * For example, you can launch periodic deadlock-detector thread here.
     * Or, any kinds of asynchronous taks such as timeout-retry, failure detector, etc.
     */
    protected abstract void startUp() throws Exception;
    
    /**
     * <b>IMPLEMENT THIS METHOD</b>
     * Bootstrapping completed and the server is up and running.
     */
    protected abstract void readyToServe() throws Exception;

    /**
     * utility method to resolve remote object
     * @param name bind name of remote object
     * @return remote object stub
     */
    @SuppressWarnings("unchecked")
    protected <T extends Remote> T lookupRemote(String name)
    throws AccessException, RemoteException, NotBoundException {
        return (T)registry.lookup(name);
    }

    /**
     * utility method to resolve remote object
     * @param id bind name of remote object
     * @return remote object stub
     */
    protected <T extends Remote> T lookupRemote(ServiceID id)
    throws AccessException, RemoteException, NotBoundException {
        return lookupRemote(id.toString());
    }

    /**
     * utility method to resolve remote object
     * @param type type of remove object
     * @return remote object stub
     */
    protected <T extends Remote> T lookupRemote(Type type,String name)
    throws AccessException, RemoteException, NotBoundException {
        return lookupRemote(new ServiceID(type,name));
    }
    
    protected <T extends Remote> Map<String,T> lookupRemote(Type type)
    throws AccessException, RemoteException, NotBoundException
    {
        Map<String,T> instances = new HashMap<String,T>();
        for ( String name : this.registry.list() ) {
            if ( type.hasType(name) ) {
                T remote = lookupRemote(name);
                ServiceID remoteID = ServiceID.parse(name);
                instances.put(remoteID.getName(), remote);
            }
        }
        return instances;
    }

    private static void deleteDirectory(File dir) throws IOException {
        File[] files = dir.listFiles();
        for ( File file : files ) {
            if ( file.isDirectory() ) {
                deleteDirectory(file);
            } else {
                if ( !file.delete() ) {
                    System.err.println("can't delete file:" + file);
                    System.exit(-1);
                }
            }
        }
        if ( !dir.delete() ) {
            System.err.println("can't delete directory:" + dir);
            System.exit(-1);
        }
    }
    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        int argc = 0;
        
        if ( args.length == 0 ) {
            System.err.println("Usage: CSEP545ServiceObject (RM|WC|TM)" +
            		" [-n|--name name]" +
            		" [-d|--dir dir]" +
            		" [-p|--port port]" +
            		" [--init]" +
            		" [--rmiHost host]" +
            		" [--rmiPort port]" +
            		" [-c|--code uri]" +
            		" ...");
            System.exit(-1);
        }
        
        // the first argument is the type of service
        String service = args[argc++];
        Type svcType = Type.valueOf(Type.class, service.toUpperCase());
        String svcName = "";
        int port = 0;
        String dir = null;
        boolean runInitialize = false;
        boolean runRecovery = true;
        
        // default RMI registry connection information
        String rmiHost = "127.0.0.1";
        int rmiPort = 1099;
        String codeBase = null;
        
        for ( ; argc < args.length; ++argc ) {
            String arg = args[argc];
            if ( "-n".equals(arg) || "--name".equals(arg) ) {
                svcName = args[++argc];
            } else if ( "-d".equals(arg) || "--dir".equals(arg) ) {
                dir = args[++argc];
            } else if ( "-p".equals(arg) || "--port".equals(arg) ) {
                port = Integer.parseInt(args[++argc]);
            } else if ( "--init".equals(arg) ) {
                runInitialize = true;
            } else if ( "--rmiHost".equals(arg) ) {
                rmiHost = args[++argc];
            } else if ( "--rmiPort".equals(arg) ) {
                rmiPort = Integer.parseInt(args[++argc]);
            } else if ( "-c".equals(arg) || "--code".equals(arg) ) {
                codeBase = args[++argc];
            } else {
                break;
            }
        }
        
        ServiceID svcID = new ServiceID(svcType,svcName);

        if ( dir == null ) {
            dir = "." + File.separatorChar + svcID + "-data";
        }
        
        System.out.println("Service ID = "+svcID);
        System.out.println("Data root directory = "+dir);
        System.out.println("Listen on port "+port);
        System.out.println("RMI registry = "+rmiHost+":"+rmiPort);
        if ( codeBase == null ) {
            codeBase = System.getProperty("java.rmi.server.codebase");
            if ( codeBase == null || codeBase.length() == 0 ) {
                codeBase = "http://127.0.0.1:8080/csep545.jar";
            }
        }
        
        System.setProperty("java.rmi.server.codebase", codeBase);
        System.out.println("RMI server codebase = "+codeBase);
        
        String[] remainArgs = argc < args.length ? Arrays.copyOfRange(args, argc, args.length) : new String[0];
        
        // initialize data directory
        File dataDir = new File(dir);
        boolean dirExist = dataDir.exists();
        if ( dirExist ) {
            if ( ! dataDir.isDirectory() ) {
                System.err.println(dataDir+" is not a directory!");
                System.exit(-1);
            }
            if ( runInitialize ) {
                // must empty directory
                System.err.println("WARN: delete and initialize "+dataDir);
                deleteDirectory(dataDir);  
                dirExist = false;
            }
        }
        
        if ( ! dirExist ) {
            if ( ! dataDir.mkdirs() ) {
                System.err.println("can't create directory: "+dataDir);
                System.exit(-1);
            }
            runInitialize = true;
            runRecovery = false;
        }
        
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        CSEP545ServiceObject server;
        if ( svcType == Type.RM ) {
            server = new MyRM();
        } else if ( svcType == Type.WC ) {
            server = new MyWC();
        } else {
            // there is a single TM
            server = new MyTM();
        }
        
        server.setServiceID(svcID);
        server.dir = dataDir;
        server.registry = LocateRegistry.getRegistry(rmiHost,rmiPort);

        // initialize service
        server.init(remainArgs);
        if ( runInitialize ) {
            server.initStorage();
        }
        if ( runRecovery ) {
            server.recovery();
        }
        
        server.startUp();
        
        // publish the server object
        Remote stub = UnicastRemoteObject.exportObject(Remote.class.cast(server),port);
        server.registry.rebind(svcID.toString(), stub);
        
        System.out.println("Serving "+svcID);
        server.readyToServe();
    }
}
