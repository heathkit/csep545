package tp.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.HashMap;
import java.util.UUID;

import tp.LM.DeadLockException;
import tp.LM;
import tp.Lockable;
import tp.Transaction;
import tp.TransactionAbortedException;

/* Class for handling persistent storage */
public class Storage<K extends Lockable & Serializable, V extends Serializable> implements Lockable {
    enum ShadowType {
       READ,
       WRITE;
    }

    class ShadowItem {
        public V value;
        public ShadowType type;
        public ShadowItem(V setValue, ShadowType setType) {
            value = setValue;
            type = setType;
        }
    }

    private final String name;
    private final File dir;

    private volatile HashMap<K, V> master;
    private HashMap<Transaction, HashMap<K, ShadowItem>> shadows;
    private transient LM myLM;

    public Storage(String set_name, File set_dir) {
        name = set_name;
        dir = set_dir;

        master = new HashMap<K, V>();
        shadows = new HashMap<Transaction, HashMap<K, ShadowItem>>();
        myLM = new MyLM();
    }

    // If we have a working copy of the resource, read that
    // otherwise just fall back to the master
    public V read(Transaction ctx, K id) throws TransactionAbortedException {
        // Get the working memory for this transaction
        HashMap<K, ShadowItem> working = getShadow(ctx);
        if (working.containsKey(id)) {
            return working.get(id).value;
        } 

        // Since we copy to working memory on read, we only need to hold the read lock
        // long enough to read
        try {
            myLM.lockForRead(ctx, id);
            V value = master.get(id);
            working.put(id, new ShadowItem(value,ShadowType.READ));
            myLM.unlockAll(ctx);
        } catch (DeadLockException e) {
            abort(ctx);
            throw new TransactionAbortedException(ctx,
                    "deadlock trying to read " + id.toString());
        } catch (InterruptedException e) {
            abort(ctx);
            throw new TransactionAbortedException(ctx,
                    "interrupted while trying to read" + id.toString());
        }

        return working.get(id).value;
    }

    // Update our working copy with the new value. Since each transaction has
    // it's own working
    // memory, there should be no need to lock it
    public boolean write(Transaction ctx, K id, V res)
            throws TransactionAbortedException {
        HashMap<K, ShadowItem> working = getShadow(ctx);
        if (working.containsKey(id)) {
            working.remove(id);
        }
        working.put(id, new ShadowItem(res, ShadowType.WRITE));

        // TODO Can this fail? Just updating memory
        return true;
    }

    public Set<K> getKeys(Transaction ctx) {
        // TODO - Grab the globalal commit lock so master doesn't change
        Set<K> result = new LinkedHashSet<K>();

        Set<K> masterKeys = master.keySet();
        if (masterKeys != null) {
            result.addAll(masterKeys);
        }

        // TODO - this also grabs deleted keys
        HashMap<K, ShadowItem> working = getShadow(ctx);
        Set<K> workingKeys = working.keySet();
        if (workingKeys != null) {
            result.addAll(workingKeys);
        }

        return result;
    }

    // Implicitly create a new shadow for this transaction if one doesn't
    // already exist.
    // This will leak memory if there are transactions that manage to neither
    // abort or commit
    private HashMap<K, ShadowItem> getShadow(Transaction ctx) {
        if (!shadows.containsKey(ctx)) {
            shadows.put(ctx, new HashMap<K, ShadowItem>());
        }
        return shadows.get(ctx);
    }

    public boolean prepare(Transaction ctx) throws TransactionAbortedException {
        // Obtain writelocks for the master copy and write all the working data
        // back into master
        try {
            HashMap<K, ShadowItem> working = getShadow(ctx);
            for (K id : working.keySet()) {
                    myLM.lockForWrite(ctx, id);

                // Delete the entry for this key in master and update with the copy
                // from working
                master.remove(id);
                if (working.get(id) != null && working.get(id).type == ShadowType.WRITE) {
                    master.put(id, working.get(id).value);
                }
            }

            myLM.lockForWrite(ctx, this);
            // Attempt to save to disk
            saveToDisk(ctx);
        } catch (DeadLockException e) {
            abort(ctx);
            throw new TransactionAbortedException(ctx,
                    "deadlock trying to write ");
        } catch (InterruptedException e) {
            abort(ctx);
            throw new TransactionAbortedException(ctx,
                    "interrupted while trying to write");
        } catch (IOException e) {
            abort(ctx);
            throw new TransactionAbortedException(ctx,
                    "Failed to save to disk while committing: " + e.toString());
        }

        return true;
    }

    public boolean commit(Transaction ctx) throws TransactionAbortedException {
        String newFile = dir.getPath() + File.separatorChar + name + "-"
                + ctx.toString() + ".master";

        // If the file for this transaction isn't found, don't update the pointer
        boolean exists = new File(newFile).exists();
        if(!exists) {
            return false;
        }

        String idxPath = dir.getPath() + File.separatorChar + name + ".idx";

        // Write the index file with new filename
        // This is assumed to be atomic, since we're only writing one string
        try { 
            FileOutputStream fos = new FileOutputStream(idxPath);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(newFile);
            oos.close();
        } catch (IOException e) {
            throw new TransactionAbortedException(ctx, "Error while updating pointer to new file");
        }

        discardPendingTransaction();
        cleanUp(ctx);
        // TODO - Delete the old file (but for debugging purposes, leave it
        // around for now)
        return true;
    }

    public boolean abort(Transaction ctx) {
        // Abort cannot fail
        cleanUp(ctx);
        return true;
    }

    private void cleanUp(Transaction ctx) {
        myLM.unlockAll(ctx);
        shadows.remove(ctx);
    }

    /*
     * Save the master db to disk. First we write a copy of the DB to a new
     * file, then we update the index that points to this master.
     */
    public void saveToDisk(Transaction ctx) throws IOException {
        // Write master to new file
        String newFile = dir.getPath() + File.separatorChar + name + "-"
                + ctx.toString() + ".master";

        FileOutputStream fos = new FileOutputStream(newFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(master);
        oos.close();

        String pendingMarker = dir.getPath() + File.separatorChar + name + ".pending";
        fos = new FileOutputStream(pendingMarker);
        oos = new ObjectOutputStream(fos);
        oos.writeObject(ctx);
        oos.close();
    }

    public Transaction getPendingTransaction() {
        Transaction pending = null;
        try {
            String pendingMarker = dir.getPath() + File.separatorChar + name + ".pending";
            FileInputStream fis = new FileInputStream(pendingMarker);
            ObjectInputStream ois = new ObjectInputStream(fis);
            pending = (Transaction) ois.readObject();
            ois.close();
        } catch( IOException e) {
            // Error reading the pending file - we should log this, but just ignore it for now
        } catch( ClassNotFoundException e) {
            // Isn't really recoverable
        }

        return pending;
    }

    public void discardPendingTransaction() {
        String pendingMarker = dir.getPath() + File.separatorChar + name + ".pending";
        new File(pendingMarker).delete();
    }

    @SuppressWarnings("unchecked")
    public void readFromDisk() throws Exception {
        // Read the index file, get path to current db
        String idxPath = dir.getPath() + File.separatorChar + name + ".idx";

        if ((new File(idxPath)).exists()) {
            FileInputStream fis = new FileInputStream(idxPath);
            ObjectInputStream ois = new ObjectInputStream(fis);
            String masterPath = (String) ois.readObject();
            ois.close();

            if ((new File(masterPath)).exists()) {
                // Read master from the file indicated by the pointer
                fis = new FileInputStream(masterPath);
                ois = new ObjectInputStream(fis);
                master = (HashMap<K, V>) ois.readObject();
                ois.close();
            }
        }
    }

    public void setDeadlockTimeout(int to) {
        myLM.setDeadlockTimeout(to);
    }
}
