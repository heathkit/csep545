package tp.impl;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import tp.Customer;
import tp.InvalidTransactionException;
import tp.RID;
import tp.RM;
import tp.TM;
import tp.Transaction;
import tp.TransactionAbortedException;
import tp.WC;

/**
 * Your Workflow Controller.
 * 
 * Rudimentary implementation is provided for your convenience.
 */
public class MyWC extends CSEP545ServiceObject implements WC {
    private static final long serialVersionUID = 5483337783924106147L;
    
    private transient volatile TM myTM;
    private transient volatile RM myCars;
    private transient volatile RM myFlights;
    private transient volatile RM myRooms;

    public MyWC()  {
    }

    @Override
    public boolean reserveItinerary(Transaction context, Customer customer, String[] flights,
            String location, boolean car, boolean room) throws RemoteException {
        
        try {
            if ( car ) {
                myCars.reserve(context, customer, RID.forCar(location));
            }
            if ( room ) {
                myRooms.reserve(context, customer, RID.forRoom(location));
            }
            
            if ( flights != null && flights.length > 0 ) {
                for ( String flight : flights ) {
                    myFlights.reserve(context, customer, RID.forFlight(flight));
                }
            }
        } catch ( Exception e ) {
            throw new RemoteException("caught a remote exception",e);
        }
        
        return true;
    }
    
    @Override
    public boolean cancelItinerary(Transaction context, Customer customer) throws RemoteException {
        try {
            myFlights.unreserve(context, customer);
            myCars.unreserve(context, customer);
            myRooms.unreserve(context, customer);
        } catch ( Exception e ) {
            throw new RemoteException("caught a remote exception",e);
        }
        return true;
    }
    
    @Override
    public int queryItineraryPrice(Transaction context, Customer customer)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        int bill = myFlights.queryReservedPrice(context, customer);
        if (myCars != myFlights) {
            bill += myCars.queryReservedPrice(context, customer);
        }
        if (myRooms != myFlights && myRooms != myCars) {
            bill += myRooms.queryReservedPrice(context, customer);
        }
        return bill;
    }

    @Override
    public String queryItinerary(Transaction context, Customer customer)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        StringBuilder buf = new StringBuilder(1024);
        buf.append( myFlights.queryReserved(context, customer));
        if (myCars != myFlights) {
            if ( buf.length() > 0 ) buf.append(',');
            buf.append( myCars.queryReserved(context, customer));
        }
        if (myRooms != myCars && myRooms != myFlights) {
            if ( buf.length() > 0 ) buf.append(',');
            buf.append( myRooms.queryReserved(context, customer));
        }
        
        return buf.toString();
    }

    @Override
    public boolean addSeats(Transaction context, String flight, int flightSeats,
            int flightPrice) throws RemoteException,
            TransactionAbortedException, InvalidTransactionException {
        return myFlights.add(context, RID.forFlight(flight), flightSeats, flightPrice);
    }
    
    @Override
    public boolean deleteSeats(Transaction context, String flight, int numSeats)
    throws RemoteException,
            TransactionAbortedException, InvalidTransactionException {
        return myFlights.delete(context, RID.forFlight(flight), numSeats);
    }

    @Override
    public boolean deleteFlight(Transaction context, String flight)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myFlights.delete(context, RID.forFlight(flight));
    }

    @Override
    public boolean addRooms(Transaction context, String location, int numRooms,
            int price) throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myRooms.add(context, RID.forRoom(location), numRooms, price);
    }

    @Override
    public boolean deleteRooms(Transaction context, String location, int numRooms)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myRooms.delete(context, RID.forRoom(location), numRooms);
    }

    @Override
    public boolean addCars(Transaction context, String location, int numCars,
            int price) throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myCars.add(context, RID.forCar(location), numCars, price);
    }

    @Override
    public boolean deleteCars(Transaction context, String location, int numCars)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myCars.delete(context, RID.forCar(location), numCars);
    }

    @Override
    public int queryFlight(Transaction context, String flight)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myFlights.query(context, RID.forFlight(flight));
    }

    @Override
    public int queryFlightPrice(Transaction context, String flight)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myFlights.queryPrice(context, RID.forFlight(flight));
    }

    @Override
    public int queryRoom(Transaction context, String location)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myRooms.query(context, RID.forRoom(location));
    }

    @Override
    public int queryRoomPrice(Transaction context, String location)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myRooms.queryPrice(context, RID.forRoom(location));
    }

    @Override
    public int queryCar(Transaction context, String location)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myCars.query(context, RID.forCar(location));
    }

    @Override
    public int queryCarPrice(Transaction context, String location)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        return myCars.queryPrice(context, RID.forCar(location));
    }

    @Override
    public String[] listFlights(Transaction context) throws RemoteException,
            TransactionAbortedException, InvalidTransactionException {
        return myFlights.listResources(context, RID.Type.FLIGHT);
    }

    @Override
    public String[] listCars(Transaction context) throws RemoteException,
            TransactionAbortedException, InvalidTransactionException {
        return myCars.listResources(context, RID.Type.CAR);
    }

    @Override
    public String[] listRooms(Transaction context) throws RemoteException,
            TransactionAbortedException, InvalidTransactionException {
        return myRooms.listResources(context, RID.Type.ROOM);
    }

    @Override
    public Customer[] listCustomers(Transaction context)
            throws RemoteException, TransactionAbortedException,
            InvalidTransactionException {
        Set<Customer> customers = new HashSet<Customer>();
        customers.addAll(Arrays.asList(myFlights.listCustomers(context)));
        customers.addAll(Arrays.asList(myCars.listCustomers(context)));
        customers.addAll(Arrays.asList(myRooms.listCustomers(context)));
        return customers.toArray(new Customer[customers.size()]);
    }

    @Override
    public Transaction start() throws RemoteException {
        return myTM.start();
    }

    @Override
    public void commit(Transaction context) throws RemoteException,
            InvalidTransactionException, TransactionAbortedException {
        myTM.commit(context);
    }

    @Override
    public void abort(Transaction context) throws RemoteException,
            InvalidTransactionException {
        myTM.abort(context);
    }

    @Override
    protected void init(String[] args) throws Exception {
        // initialize all remote references
        refresh();
    }

    @Override
    protected void initStorage() throws Exception {
    }

    @Override
    protected void recovery() throws Exception {
    }

    @Override
    protected void startUp() throws Exception {
    }

    @Override
    protected void readyToServe() throws Exception {
    }

    /**
    * <b>IMPLEMENT THIS METHOD</b>
    * if you want to implement a different service architecture.
    */
    @Override
    public void refresh() throws RemoteException, NotBoundException {
        // initialize the TM
        myTM = this.lookupRemote(ServiceID.forTM());
        
        // initialize all RMs (if there's only one,
        // then assign each RM variable to it)
        Map<String,RM> rms = this.lookupRemote(Type.RM);
        myCars = rms.containsKey("car") ? rms.get("car") : rms.get("");
        myFlights = rms.containsKey("flight") ? rms.get("flight") : rms.get("");
        myRooms = rms.containsKey("room") ? rms.get("room") : rms.get("");
    }
}
