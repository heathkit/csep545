package tp.impl;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.HashSet;
import java.util.Set;

import tp.Customer;
import tp.InvalidTransactionException;
import tp.RID;
import tp.RM;
import tp.TM;
import tp.Transaction;
import tp.TransactionAbortedException;

/**
 * Your Resource Manager.
 * 
 * Rudimentary implementation is provided for your convenience.
 * <h3>Command line arguments</h3>
 * 
 * 
 * The following arguments are further processed by this class
 * in addition to common arguments processed by {@link CSEP545ServiceObject#main(String[])}.
 * 
 * <ul>
 * <li><code>-x</code> or <code>--selfdestruct</code> <code>numio</code>:
 * Set the self destruction timer on this RM. If this argument is given,
 * the RM MUST call System.exit() <i>immediately</i> after numio disk operations.
 * (default: 0. no self destruction)</li>
 * <li><code>-t</code> or <code>--timeout</code> <code>s</code>: 
 * Set deadlock detection timeout to <code>s</code> seconds. 
 * (default: {@link tp.LM#DEFAULT_DEADLOCK_TIMEOUT})</li>
 * </ul>
 * 
 * @see tp.impl.CSEP545ServiceObject
 */

public class MyRM extends CSEP545ServiceObject implements RM {
    private static final long serialVersionUID = -824882060186317662L;

    private transient volatile TM myTM;
    private int selfDestructCounter = -1;
    
    /**
     * Inventory information. Should be part of your database.
     * Intentionally using {@link java.util.HashMap} to increase the chance of
     * incorrect synchronization using Lock Manager.
     */
    private Storage<RID,Resource> resources;
    
    /**
     * Reservation information. Should be part of your database.
     * Intentionally using {@link java.util.HashMap} to increase the chance of
     * incorrect synchronization using Lock Manager.
     */
    private Storage<Customer,HashSet<RID>> reservations;

    public MyRM() {
    }

    @Override
    public void commit(Transaction xid)
    throws InvalidTransactionException,
            TransactionAbortedException, RemoteException {
        selfDestructCountdown();
        resources.commit(xid);
        reservations.commit(xid);
    }

    @Override
    public void abort(Transaction xid) throws RemoteException {
        resources.abort(xid);
        reservations.abort(xid);
    }

    @Override
    public boolean prepare(Transaction xid) 
    throws InvalidTransactionException,
            TransactionAbortedException, RemoteException {
        return (resources.prepare(xid) && reservations.prepare(xid));
    }

    @Override
    public boolean add(Transaction xid, RID i, int count, int price)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(xid, getName());
        
        Resource res = resources.read(xid, i);
        if ( res == null ) {
            res = new Resource(i,count,price);
        } else {
            res.incrCount(count);
            res.setPrice(price);
        }
        return resources.write(xid, i, res);
    }

    @Override
    public boolean delete(Transaction xid, RID rid)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(xid, getName());
        
        Resource removed = resources.read(xid, rid);

        // drop all reservations on removed resource
        if ( removed != null ) {
            resources.write(xid, rid, null);

            for ( Customer cust : reservations.getKeys(xid) ) {
                HashSet<RID> reserved_ids = reservations.read(xid, cust);
                reserved_ids.remove(rid);
                reservations.write(xid, cust, reserved_ids);
            }
        }
        
        return removed != null;
    }
    
    @Override
    public boolean delete(Transaction xid, RID rid, int count)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(xid, getName());
        
        Resource removed = resources.read(xid, rid);
        if ( removed != null ) {
            if ( removed.getCount() > count ) {
                removed.decrCount(count);
                return resources.write(xid, rid, removed);
            } else {
                return delete(xid, rid);
            }
        }
        return true;
    }

    @Override
    public int query(Transaction xid, RID rid)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(xid, getName());
        
        Resource resource = resources.read(xid, rid);
        if ( resource == null ) {
            throw new IllegalArgumentException(rid+" does not exist");
        }
        return resource.getCount();
    }

    @Override
    public int queryPrice(Transaction xid, RID rid)
    throws TransactionAbortedException, InvalidTransactionException, RemoteException {
        myTM.enlist(xid, getName());
        
        Resource resource = resources.read(xid, rid);
        if ( resource == null ) {
            throw new IllegalArgumentException(rid+" does not exist");
        }
        return resource.getPrice();
    }

    @Override
    public String queryReserved(Transaction context, Customer customer)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(context, getName());
        
        StringBuilder buf = new StringBuilder(512);
        
        Set<RID> reserved = reservations.read(context, customer);
        if ( reserved != null ) {
            for ( RID rid : reserved ) {
                if ( buf.length() > 0 ) {
                    buf.append(',');
                }
                buf.append(rid);
            }
        }
        return buf.toString();
    }

    @Override
    public int queryReservedPrice(Transaction context, Customer customer)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(context, getName());
        
        int bill = 0;
        
        Set<RID> reserved = reservations.read(context, customer);
        if ( reserved != null ) {
            for ( RID rid : reserved ) {
                Resource r = resources.read(context, rid);
                if ( r == null ) {
                    throw new IllegalStateException(rid+" does not exist in RM");
                }
                bill += r.getPrice();
            }
        }
        
        return bill;
    }

    @Override
    public boolean reserve(Transaction context, Customer customer, RID id)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(context, getName());
        
        Resource resource = resources.read(context, id);
        
        if ( resource == null ) {
            throw new IllegalArgumentException(id+" does not exist!");
        }
        if ( resource.getCount() == 0 ) {
            return false;
        }
        
        HashSet<RID> reserved = reservations.read(context, customer);
        if ( reserved == null ) {
            reserved = new LinkedHashSet<RID>();
            reserved.add(resource.getID());
        } else {
            reserved.add(resource.getID());
        }
        
        resource.decrCount();

        resources.write(context, id, resource);
        reservations.write(context, customer, reserved);
        
        return true;
    }

    @Override
    public void unreserve(Transaction context, Customer customer)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(context, getName());
        
        Set<RID> reserved = reservations.read(context, customer);
        if ( reserved == null ) {
            // silently discard
        } else {
            // TODO need to add lock
            for ( RID rid : reserved ) {
                Resource resource = resources.read(context, rid);
                if ( resource == null ) {
                    // FIXME warn that the rID does not exist!
                } else {
                    resource.incrCount();
                    resources.write(context, rid, resource);
                }
            }
            reservations.write(context, customer, null);
        }
    }

    @Override
    public String[] listResources(Transaction context, RID.Type type)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(context, getName());
        
        // TODO - need appropriate global lock to list resources
        ArrayList<String> result = new ArrayList<String>();
        for ( RID id : resources.getKeys(context) ) {
            Resource resource = resources.read(context, id);
            if ( resource != null && type == resource.getType() ) {
                result.add(resource.toString());
            }
        }
        return result.toArray(new String[0]);
    }

    @Override
    public Customer[] listCustomers(Transaction context)
    throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        myTM.enlist(context, getName());
        
        // TODO - need appropriate global lock to list customers
        return reservations.getKeys(context).toArray(new Customer[0]);
    }

    private void selfDestructCountdown() {
        if( selfDestructCounter > 0 ) { 
          selfDestructCounter--;
          if( selfDestructCounter == 0 ) {
            System.exit(1);
          }
        }
    }

    @Override
    public void shutdown() throws RemoteException {
        // TODO Do proper shutdown here
    }

    @Override
    public void selfDestruct(int diskWritesToWait) throws RemoteException {
        selfDestructCounter = diskWritesToWait;
    }
    
    @Override
    public String getName() throws RemoteException {
        return getServiceID().toString();
    }

    /**
     * @todo setup {@link #selfDestruct(int)} here
     */
    @Override
    protected void init(String[] args) throws Exception {
        int argc = 0;
        
        // Initialize resources and reservations here
        resources = new Storage<RID,Resource>("resources", getDataRoot());
        reservations = new Storage<Customer, HashSet<RID>>("reservations", getDataRoot());
        
        for ( ; argc < args.length; ++argc ) {
            String arg = args[argc];
            if ( "-x".equals(arg) || "--selfdestruct".equals(arg) ) {
                int nio = Integer.parseInt(args[++argc]);
                System.out.println("Setting self-destruction counters to "+nio);
                selfDestruct(nio);
            } else if ( "-t".equals(arg) || "--timeout".equals(arg) ) {
                int to = Integer.parseInt(args[++argc]);
                System.out.println("Setting deadlock timeout to "+to+" seconds");
                resources.setDeadlockTimeout(to);
                reservations.setDeadlockTimeout(to);
            } else {
                throw new IllegalArgumentException("unknown argument: "+arg);
            }
        }

        refresh();
    }

    @Override
    protected void initStorage() throws Exception {
      // Nothing needed here
    }

    @Override
    protected void recovery() throws Exception {
        Transaction pending = resources.getPendingTransaction();
        if(pending != null) {
            if(myTM.isTransactionCommitted(pending)) {
                resources.commit(pending);
            } else {
                resources.discardPendingTransaction();
            }
        }

        pending = reservations.getPendingTransaction();
        if(pending != null) {
            if(myTM.isTransactionCommitted(pending)) {
                resources.commit(pending);
            } else {
                resources.discardPendingTransaction();
            }
        }

        resources.readFromDisk();
        reservations.readFromDisk();
    }

    @Override
    protected void startUp() throws Exception {
        // TODO deadlock detector, retry timeout
    }
    
    @Override
    protected void readyToServe() throws Exception {
        // register this resource manager to TM
        myTM.register(getName());
    }

    @Override
    public void refresh() throws RemoteException, NotBoundException {
        myTM = this.lookupRemote(ServiceID.forTM());
    }
}
