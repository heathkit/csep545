package tp.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import tp.InvalidTransactionException;
import tp.RM;
import tp.TM;
import tp.Transaction;
import tp.TransactionAbortedException;

/**
 * <h3>Your Transaction Manager</h3>
 *
 * A rudimentary implementation is provided for your convenience.
 */
public class MyTM extends CSEP545ServiceObject implements TM {
    private static final long serialVersionUID = -3864279262065640856L;

    /**
     * keep track of resource managers
     */
    private transient Map<String, RM> resourceManagers;
    
    /**
     * keep track of transactions per RM
     */
    private volatile Map<Transaction, Set<String>> transactionMap;
    
    /**
     * keep track of transaction decisions
     */
    private volatile Map<Transaction, Boolean> decidedTransactions;

    public MyTM() {
        resourceManagers = Collections.synchronizedMap(new HashMap<String, RM>());
        transactionMap = Collections.synchronizedMap(new HashMap<Transaction, Set<String>>());
        decidedTransactions = Collections.synchronizedMap(new HashMap<Transaction, Boolean>());
    }
    
    @Override
    public Transaction start() throws RemoteException {
        Transaction trans = new Transaction();
        
        if (!transactionMap.containsKey(trans)) {
            transactionMap.put(trans, new HashSet<String>());
        }
        
        return trans;
    }

    @Override
    public void commit(Transaction xid) throws InvalidTransactionException,
            TransactionAbortedException, RemoteException {
        // Get the list of participants
        Set<String> rmSet = transactionMap.get(xid);
        if (rmSet == null) {
            throw new IllegalStateException("RM list should not be null for a transaction");
        }
        
        // Two-phase commit
        
        // Phase 1: Prepare
        boolean toCommit = true; 
        for (String rmId : rmSet) {
            RM rm = resourceManagers.get(rmId);
            if (rm != null) {
                try {
                    // Request for prepare
                    if (!rm.prepare(xid)) {
                        toCommit = false;
                    }
                }
                catch (Exception e) {
                    toCommit = false; 
                }
            }
        }
        
        // Phase 2: Decision
        // Log the decision
        logDecision(xid, toCommit);
        
        if (toCommit) {            
            // Commit
            internalCommit(xid);
        }
        else {
            // Abort
            internalAbort(xid);
        }
    }
    
    private void internalCommit(Transaction xid) {
        // Get the list of participants
        Set<String> rmSet = transactionMap.get(xid);
        if (rmSet == null) {
            throw new IllegalStateException("RM list should not be null for a transaction");
        }
        
        // Notify each participant to commit
        Set<String> successfullyCommittedRms = new HashSet<String>();
        for (String rmId : rmSet) {
            RM rm = resourceManagers.get(rmId);
            if (rm != null) {
                // Commit
                try {
                    rm.commit(xid);
                    successfullyCommittedRms.add(rmId);
                }
                catch (Exception e) {
                    // Do nothing
                }
            }
        }
        
        // Remove the participants who successfully committed
        for (String rmId : successfullyCommittedRms) {
            rmSet.remove(rmId);
        }

        // Forget the transaction if rmSet is empty
        if (rmSet.isEmpty()) {
            forgetTransaction(xid);
        }

        System.out.println(xid+" : COMMIT");        
    }

    @Override
    public void abort(Transaction xid) throws RemoteException {
        // Log the decision
        logDecision(xid, false);
        // Abort
        internalAbort(xid);
    }
    
    private void internalAbort(Transaction xid) {
        // Get list of participants
        Set<String> rmSet = transactionMap.get(xid);
        if (rmSet == null) {
            throw new IllegalStateException("RM list should not be null for a transaction");
        }

        // Notify each participant to abort
        Set<String> successfullyAbortedRms = new HashSet<String>();
        for (String rmId : rmSet) {
            RM rm = resourceManagers.get(rmId);
            if (rm != null) {
                // Abort
                try {
                    rm.abort(xid);
                    successfullyAbortedRms.add(rmId);
                }
                catch (Exception e) {
                    // Do nothing
                }
            }
        }
        
        // Remove the participants who successfully committed
        for (String rmId : successfullyAbortedRms) {
            rmSet.remove(rmId);
        }

        // Forget the transaction if rmSet is empty
        if (rmSet.isEmpty()) {
            forgetTransaction(xid);
        }
        
        System.out.println(xid+" : ABORT");
    }

    @Override
    public boolean enlist(Transaction xid, String enlistingRM)
            throws TransactionAbortedException, InvalidTransactionException,
            RemoteException {
        if (transactionMap.containsKey(xid)) {
            // Map contains the transaction
            Set<String> rmSet = transactionMap.get(xid);
            if (rmSet == null) {
                // Fail silently
                return false;
            }
            else {
                if (rmSet.contains(enlistingRM)) {
                    // Set already contains the RM
                    return true;
                }
                else {
                    // Set doesn't contain the RM. So add it.
                    boolean result = rmSet.add(enlistingRM);
                    if (result) {
                        try {
                            // Save to disk for recovery
                            saveTransactionMapToDisk();
                        }
                        catch (IOException e) {
                            return false;
                        }
                    }
                    return result;
                }
            }
        }
        else {
            // Map doesn't contain the transaction. Fail silently
            return false;
        }
    }
    
    private void logDecision(Transaction xid, Boolean decision) {
        // Add the committed transaction to the list
        decidedTransactions.put(xid, decision);
        try {
            // Save to disk for recovery
            saveDecidedTransactionsToDisk();
        }
        catch (IOException e) {
            // Silently fail
        }
    }
    
    private void forgetTransaction(Transaction xid) {
        // Forget the transaction
        transactionMap.remove(xid);
        try {
            saveTransactionMapToDisk();
        }
        catch (IOException e) {
            // Silently fail
        }
        
        decidedTransactions.remove(xid);
        try {
            saveDecidedTransactionsToDisk();
        }
        catch (IOException e) {
            // Silently fail
        }
    }
    
    private void saveTransactionMapToDisk() throws IOException {
        saveObjectToDisk(transactionMap, "transactionMap");
    }
    
    @SuppressWarnings("unchecked")
    private Map<Transaction, Set<String>> readTransactionMapFromDisk() throws IOException, ClassNotFoundException {
        return (Map<Transaction, Set<String>>) readObjectFromDisk("transactionMap");
    }
    
    private void saveDecidedTransactionsToDisk() throws IOException {
        saveObjectToDisk(decidedTransactions, "decidedTransactions");
    }
    
    @SuppressWarnings("unchecked")
    private Map<Transaction, Boolean> readDecidedTransactionsFromDisk() throws IOException, ClassNotFoundException {
        return (Map<Transaction, Boolean>) readObjectFromDisk("decidedTransactions");
    }
    
    private void saveObjectToDisk(Object obj, String name) throws IOException {
        // Write object to new file
        String newFile = dir.getPath() + File.separatorChar + name + "-"
                + UUID.randomUUID().toString() + ".master";
        String idxPath = dir.getPath() + File.separatorChar + name + ".idx";

        FileOutputStream fos = new FileOutputStream(newFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(obj);
        oos.close();

        // Write the index file with new filename
        fos = new FileOutputStream(idxPath);
        oos = new ObjectOutputStream(fos);
        oos.writeObject(newFile);
        oos.close();
    }

    private Object readObjectFromDisk(String name) throws IOException, ClassNotFoundException {
        // Read the index file, get path to current db
        String idxPath = dir.getPath() + File.separatorChar + name + ".idx";

        if ((new File(idxPath)).exists()) {
            FileInputStream fis = new FileInputStream(idxPath);
            ObjectInputStream ois = new ObjectInputStream(fis);
            String masterPath = (String) ois.readObject();
            ois.close();

            if ((new File(masterPath)).exists()) {
                // Read master from the file indicated by the pointer
                fis = new FileInputStream(masterPath);
                ois = new ObjectInputStream(fis);
                Object result = ois.readObject();
                ois.close();
                
                return result;
            }
        }
        
        return null;
    }

    // RELATED TO SERVICE
    
    @Override
    public boolean isTransactionCommitted(Transaction ctx) {
        if(! decidedTransactions.containsKey(ctx)) {
            return false;
        }
        return decidedTransactions.get(ctx);
    }
    
    /**
     * look up RMI registry to cache remote reference of
     * RM represented by <code>rm</code>. This makes easier to
     * coordinate two-phase commit.
     * @param rm the name of RM to register
     */
    @Override
    public void register(String rm) throws RemoteException {
        System.out.println("Register "+rm);
        try {
            ServiceID remote = ServiceID.parse(rm);
            if ( remote.getType() != Type.RM )
                throw new IllegalArgumentException(rm+" is not a resource manager!");
            
            RM newRM = lookupRemote(rm);
            resourceManagers.put(rm, newRM);
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void shutdown() throws RemoteException {
    }

    @Override
    protected void init(String[] args) throws Exception {
    }

    @Override
    protected void initStorage() throws Exception {
    }

    @Override
    protected void recovery() throws Exception {
        // Recover data structures
        Map<Transaction, Set<String>> recoveredTransactionMap = readTransactionMapFromDisk();
        if (recoveredTransactionMap != null) {
            transactionMap = recoveredTransactionMap;
        }
        
        Map<Transaction, Boolean> recoveredDecidedTransactions = readDecidedTransactionsFromDisk();
        if (recoveredDecidedTransactions != null) {
            decidedTransactions = recoveredDecidedTransactions;
        }
    }

    @Override
    protected void startUp() throws Exception {
    }

    @Override
    protected void readyToServe() throws Exception {
    }

    @Override
    public void refresh() throws RemoteException, NotBoundException {
        // do not need to do anything since all RM will
        // call register again on recovery
    }
}
