package tp;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Resource Manager Interface
 */
public interface RM extends Remote {

    /**
     * get the name of this RM
     * @return the name of this RM
     */
    String getName() throws RemoteException;
    
    /**
     * prepares itself to commit a transaction
     * @param xid transaction identifier
     * @return true if ready to commit, else false
     * @throws RemoteException
     */
    boolean prepare(Transaction xid)
    throws InvalidTransactionException, TransactionAbortedException, RemoteException;

    /**
     * commit transaction
     * @param xid transaction identifier
     */
    void commit(Transaction xid)
    throws InvalidTransactionException, TransactionAbortedException, RemoteException;

    /**
     * abort transaction
     * @param xid transaction identifier
     */
    void abort(Transaction xid)
    throws RemoteException;

    /**
     * add <code>qty</code> items described by <code>resource</code>. 
     * If <code>resource</code> does not exist in the RM, create a new one.
     * 
     * @param xid transaction identifier
     * @param resource   resource identifier
     * @param qty quantity (seats, cars, rooms)
     * @param price price per unit
     * @return <code>true</code> on success. <code>false</code> otherwise
     * @see tp.WC#addSeats(Transaction, String, int, int)
     * @see tp.WC#addCars(Transaction, String, int, int)
     * @see tp.WC#addRooms(Transaction, String, int, int)
     */
    boolean add(Transaction xid,RID resource,int qty,int price)
    throws TransactionAbortedException, 
          InvalidTransactionException, 
          RemoteException;

    /**
     * Drop <code>resource</code> from this RM.
     * <i>All reservations on <code>resource</code> must be dropped as well.</i>
     * @param xid transaction identifier
     * @param resource   resource identifier
     * @see tp.WC#deleteFlight(Transaction, String)
     */
    boolean delete(Transaction xid,RID resource)
    throws TransactionAbortedException, 
          InvalidTransactionException, 
          RemoteException;

    /**
     * Remove <i>exactly</i> <code>qty</code> <i>unreserved</i> <code>resource</code> from this RM.
     * @param xid   transaction identifier
     * @param resource     resource identifier
     * @param qty number of resource to remove. <code>-1</code> removes all remaining
     * @return <code>true</code> on successful deletion.
     * If there is no such <code>resource</code>, return <code>true</code>.
     * <code>false</code> otherwise.
     * If there are not enough <code>qty</code> to remove, return <code>false</code>.
     * @see tp.WC#deleteSeats(Transaction, String, int)
     * @see tp.WC#deleteCars(Transaction, String, int)
     * @see tp.WC#deleteRooms(Transaction, String, int)
     */
    boolean delete(Transaction xid,RID resource,int qty)
    throws TransactionAbortedException, 
            InvalidTransactionException, 
            RemoteException;
    
    /**
     * query: equivalent to queryCars, queryFlights, queryRooms
     * @param xid transaction identifier
     * @param resource   resource identifier
     * @return available quantity
     * @see tp.WC#queryCar(Transaction, String)
     * @see tp.WC#queryRoom(Transaction, String)
     * @see tp.WC#queryFlight(Transaction, String)
     */
    int query(Transaction xid,RID resource)
    throws TransactionAbortedException, 
          InvalidTransactionException, 
          RemoteException;
    /**
     * query: equivalent to queryCarsPrice, queryFlightsPrice, queryRoomsPrice
     * @param xid transaction identifier
     * @param resource   resource identifier
     * @return price per unit for requested item <code>resource</code>
     * @see tp.WC#queryCarPrice(Transaction, String)
     * @see tp.WC#queryRoomPrice(Transaction, String)
     * @see tp.WC#queryFlightPrice(Transaction, String)
     */
    int queryPrice(Transaction xid,RID resource)
    throws TransactionAbortedException, 
          InvalidTransactionException, 
          RemoteException;

   /**
     * Get the bill for the <code>customer</code>
     * return a string representation of reservations
     * @param context the transaction ID
     * @param customer the customer ID
     * @return a string representation of reservations
     * @see tp.WC#queryItinerary(Transaction, Customer)
     */
   String queryReserved(Transaction context,Customer customer)
    throws TransactionAbortedException, 
          InvalidTransactionException, 
          RemoteException;

    /**
     * Get the total amount of money the <code>customer</code> owes
     * in this RM.
     * @param context the transaction ID
     * @param customer the customer ID
     * @return total price of all reservations
     * @see tp.WC#queryItineraryPrice(Transaction, Customer)
     */
    int queryReservedPrice(Transaction context,Customer customer)
    throws TransactionAbortedException, 
          InvalidTransactionException, 
          RemoteException;

    /**
     * reserve a resource <code>i</code> on behalf of customer <code>c</code>
     * @param context the transaction ID
     * @param customer the customer ID
     * @param resource resource to reserve
     * @return <code>true</code> on successful reservation. <code>false</code> otherwise.
     * @see tp.WC#reserveItinerary(Customer, String[], String, boolean, boolean)
     */
    boolean reserve(Transaction context,Customer customer,RID resource)
    throws TransactionAbortedException, 
          InvalidTransactionException, 
          RemoteException;

     /**
     * drop all reserved resources for customer <code>customer</code>
     * @param context the transaction ID
     * @param customer the customer ID
     * @see tp.WC#cancelItinerary(Customer)
     */
    void unreserve(Transaction context,Customer customer)
    throws TransactionAbortedException, 
          InvalidTransactionException, 
          RemoteException;

    /**
     * list of resources
     * @param context
     * @param type listing resource type
     * @return array of comma separated strings of available resource information.
     * The format of each string is following:
     * <p>
     * <i>resource name ',' available quantity ',' price</i>
     * </p>
     * @see tp.WC#listCars(Transaction)
     * @see tp.WC#listFlights(Transaction)
     * @see tp.WC#listRooms(Transaction)
     */
    String[] listResources(Transaction context,RID.Type type)
    throws TransactionAbortedException, 
            InvalidTransactionException, 
            RemoteException;

    /**
     * list of customers who reserve resources managed by this RM
     * @param context the transaction ID
     * @return list of customers
     * @see tp.WC#listCustomers(Transaction)
     */
    Customer[] listCustomers(Transaction context)
    throws TransactionAbortedException, 
            InvalidTransactionException, 
            RemoteException;

    /**
     * Exit (simulate a failure) after a specified number of disk writes.
     * Support for this method requires a wrapper around the system's
     * write system call that decrements the counter set by this method.
     * This counter should be set to zero by default, which makes the
     * wrapper doing nothing. If the counter is non-zero, the wrapper
     * should decrement it, see if it is zero, and if so call exit().
     * This method is not part of a transaction. It is intended to
     * simulate an RM failure.
     * @param diskWritesToWait number of disk writes to wait until terminate
     */
    void selfDestruct(int diskWritesToWait) throws RemoteException;
}
