package tp;

/**
 * A marker interface to indicate the type of
 * lockable item. A Lockable item must properly implement
 * {@link Object#hashCode()}, {@link Object#equals(Object)}, and 
 * {@link Object#toString()} methods to be compatible with
 * {@link LM}.
 */
public interface Lockable {
}
