package tp;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Workflow Controller stub
 */
public interface WC extends Remote
{
    /**
     * Reserve an itinerary
     * @param context the transaction ID
     * @param customer the customer
     * @param flights an integer array of flight numbers
     * @param location travel location
     * @param car true if car reservation is needed
     * @param room true if a room reservation is needed
     * @return <code>true</code> on success, <code>false</code> otherwise.
     * @see tp.RM#reserve(Transaction, Customer, RID)
     */
    boolean reserveItinerary(Transaction context, Customer customer,String[] flights,String location,boolean car,boolean room) throws RemoteException;

    /**
     * Cancel an itinerary owned by <code>customer</code>
     * @param context the transaction ID
     * @param customer the customer
     * @return <code>true</code> on success, <code>false</code> otherwise.
     * @see tp.RM#unreserve(Transaction, Customer)
     */
    boolean cancelItinerary(Transaction context, Customer customer) throws RemoteException;
    
    /**
     * Get the bill for the <code>customer</code>
     * @param context the transaction ID
     * @param customer the customer ID
     * @return a string representation of reservations
     * @see tp.RM#queryReserved(Transaction, Customer)
     */
    String queryItinerary(Transaction context, Customer customer)
        throws RemoteException, 
              TransactionAbortedException, 
              InvalidTransactionException;

    /**
     * Get the total amount of money the <code>customer</code> owes
     * @param context the transaction ID
     * @param customer the customer ID
     * @return total price of reservations
     * @see tp.RM#queryReserved(Transaction, Customer)
     */
    int queryItineraryPrice(Transaction context, Customer customer)
    throws RemoteException,
              TransactionAbortedException, 
              InvalidTransactionException;

    /**
     * Add seats to a flight
     * This method will be used to create a new flight but 
     * if the flight already exists, seats will be added 
     * and the price overwritten
     * @param context the transaction ID
     * @param flight a flight number
     * @param flightSeats the number of  flight Seats
     * @param flightPrice price per seat
     * @return <code>true</code> on success, <code>false</code> otherwise.
     * @see tp.RM#add(Transaction, RID, int, int)
     */
    boolean addSeats(Transaction context, String flight, int flightSeats, int flightPrice)
        throws RemoteException, TransactionAbortedException, InvalidTransactionException;

    /**
     * delete seats from a <code>flight</code>
     * @param context the transaction ID
     * @param flight a flight number
     * @param numSeats the number of  flight Seats
     * @return <code>true</code> on success, <code>false</code> otherwise.
     * @see tp.RM#delete(Transaction,RID,int)
     */
    boolean deleteSeats(Transaction context, String flight, int numSeats)
        throws RemoteException, TransactionAbortedException, InvalidTransactionException;

    /**
     * Delete the entire flight. 
     * deleteFlight implies whole deletion of the flight, all seats, all reservations.
     * @param context the transaction ID
     * @param flight the flight number
     * @return <code>true</code> on success, <code>false</code> otherwise.
     * @see tp.RM#delete(Transaction,RID)
     */
    boolean deleteFlight(Transaction context, String flight)
    throws RemoteException, TransactionAbortedException, InvalidTransactionException;

    /**
     * Add rooms to a location. 
     * This should look a lot like addFlight,
     * only keyed on a string location instead of a flight number.
     * @param context the transaction ID
     * @param location the location to add rooms
     * @param numRooms number of rooms to add
     * @param price room price
     * @return <code>true</code> on success, <code>false</code> otherwise.
     * @see tp.RM#add(Transaction, RID, int, int)
     */
    boolean addRooms(Transaction context, String location, int numRooms, int price)
    throws RemoteException, TransactionAbortedException, InvalidTransactionException;

    /**
     * Delete rooms. 
     * @param context the transaction ID
     * @param location the location to add rooms
     * @param numRooms the number of rooms to delete
     * @return <code>true</code> on success, <code>false</code> otherwise.
     * @see tp.RM#delete(Transaction,RID,int)
     */
    boolean deleteRooms(Transaction context, String location, int numRooms)
    throws RemoteException, TransactionAbortedException, InvalidTransactionException;

    /**
     * Add cars to a location. 
     * This should look a lot like addFlight,
     * only keyed on a string location instead of a flight number.
     * @param context the transaction ID
     * @param location the location to add cars
     * @param numCars number of cars to add
     * @param price rental price
     * @return <code>true</code> on success, <code>false</code> otherwise.
     * @see tp.RM#add(Transaction, RID, int, int)
     */
    boolean addCars(Transaction context, String location, int numCars, int price)
	throws RemoteException, 
	       TransactionAbortedException,
	       InvalidTransactionException; 

    /**
     * Delete cars. 
     * @param context the transaction ID
     * @param location the location to add cars
     * @param numCars the number of cars to delete
     * @return <code>true</code> on success, <code>false</code> otherwise.
     * @see tp.RM#delete(Transaction,RID,int)
     */
    boolean deleteCars(Transaction context, String location, int numCars)
	throws RemoteException, 
	       TransactionAbortedException,
	       InvalidTransactionException; 

    /**
     * Get the number of seats available. 
     * return the number of seats available
     * @param context the transaction ID
     * @param flight the flight number
     * @return the number of seats available. negative value if the flight does not exists
     * @see tp.RM#query(Transaction, RID)
     */
    int queryFlight(Transaction context, String flight)
	throws RemoteException, 
	       TransactionAbortedException,
	       InvalidTransactionException; 

    /**
     * Get the flight price. 
     * return the price
     * @param context the transaction ID
     * @param flight the flight number
     * @return the price. Negative value if the flight does not exists
     * @see tp.RM#queryPrice(Transaction, RID)
     */
    int queryFlightPrice(Transaction context, String flight)
	throws RemoteException, 
	       TransactionAbortedException,
	       InvalidTransactionException; 

    /**
     * Get the number of rooms available. 
     * return the number of rooms available
     * @param context the transaction ID
     * @param location the rooms location
     * @return the number of rooms available
     * @see tp.RM#query(Transaction, RID)
     */
    int queryRoom(Transaction context, String location)
	throws RemoteException, 
	       TransactionAbortedException,
	       InvalidTransactionException; 

    /**
     * Get the room price. 
     * @param context the transaction ID
     * @param location the rooms location
     * @return the price
     * @see tp.RM#queryPrice(Transaction, RID)
     */
    int queryRoomPrice(Transaction context, String location)
	throws RemoteException, 
	       TransactionAbortedException,
	       InvalidTransactionException; 

    /**
     * Get the number of cars available. 
     * @param context the transaction ID
     * @param location the cars location
     * @return the number of cars available
     * @see tp.RM#query(Transaction, RID)
     */
    int queryCar(Transaction context, String location)
	throws RemoteException, 
	       TransactionAbortedException,
	       InvalidTransactionException; 

    /**
     * Get the cars  price. 
     * @param context the transaction ID
     * @param location the cars location
     * @return the price
     * @see tp.RM#queryPrice(Transaction, RID)
     */
    int queryCarPrice(Transaction context, String location)
	throws RemoteException, 
	       TransactionAbortedException,
	       InvalidTransactionException; 

    /**
     * list existing flights
     * @param context transaction id
     * @return list of existing flights
     * @see #addSeats(Transaction, String, int, int)
     * @see tp.RM#listResources(Transaction, RID.Type)
     */
    String[] listFlights(Transaction context)
    throws RemoteException, 
            TransactionAbortedException,
            InvalidTransactionException; 

    /**
     * list existing cars
     * @param context transaction id
     * @return list of existing cars
     * @see #addCars(Transaction, String, int, int)
     * @see tp.RM#listResources(Transaction, RID.Type)
     */
    String[] listCars(Transaction context)
    throws RemoteException, 
            TransactionAbortedException,
            InvalidTransactionException; 

    /**
     * list existing rooms
     * @param context transaction id
     * @return list of existing rooms
     * @see #addRooms(Transaction, String, int, int)
     * @see tp.RM#listResources(Transaction, RID.Type)
     */
    String[] listRooms(Transaction context)
    throws RemoteException, 
            TransactionAbortedException,
            InvalidTransactionException; 

    /**
     * list existing customers that have itinerary
     * @param context transaction id
     * @return list of existing customers that have itineraries
     * @see #reserveItinerary(Customer, String[], String, boolean, boolean)
     * @see tp.RM#listCustomers(Transaction)
     */
    Customer[] listCustomers(Transaction context)
    throws RemoteException, 
            TransactionAbortedException,
            InvalidTransactionException; 

    // RELATED TO TRANSACTION
    
    /**
     * Start a transaction
     * return a unique transaction ID
     * @return a new transaction identifier
     */
    Transaction start() throws RemoteException;

    /**
     * Commit a transaction
     * @param context the transaction ID
     */
    void commit(Transaction context)
    throws RemoteException, InvalidTransactionException, TransactionAbortedException;

    /**
     * Abort a transaction
     * @param context the transaction ID
     */
    void abort (Transaction context)
    throws RemoteException, InvalidTransactionException;
}
